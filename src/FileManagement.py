# coding=utf-8
import time
import telepot
import telebot
import commands
import random
import traceback
import time
from datetime import date 
import operator
global bot
bot = telepot.Bot('133326326:AAHCjdG48vALFV-8iqmIammHMbHWcEkx-mM')

# resources directory path di base
res = "resources/"

		
"""
	Variante della funzione randomphoto, utilizzata per inviare file .mp4
	Per maggiori dettagli, leggere la doc di randomphoto
"""
def randomFile(comando,chat):
	parole_chiave = ['brando','Brando','Simba','simba','Serena','serena','Raiden','raiden','Halomeme','halomeme']
	for i in range(len(parole_chiave)):
		if comando.startswith(parole_chiave[i]) or comando.startswith(parole_chiave[i]):
			comando = parole_chiave[i]
			comando = comando.title()
			numcasuale = random.randint(1,18)
			while True:
				File = comando + str(numcasuale) + '.mp4'
				path = res + 'randomphotos/' + comando + '/Video_gif/' + File
				try:
					f = open(path,'rb')
					bot.sendMessage(chat,"gif/video di " + comando + " in arrivo")
					bot.sendDocument(chat,f)
					print '>>>>Inviata correttamente la gif/video richiesta<<<< ==> ' + comando
					return
				except:
					numcas = random.randint(1,10)
					numcasuale -= numcas
					if numcasuale <= 0:
						numcasuale = random.randint(1,5)
"""
	La seguente funzione prende come parametri il contenuto del messaggio, l'id della chat in cui viene chiamata la funzione e una didascalia per l'immagine inviata.
	
	Vi è un array di keywords disponibili per la ricerca e l'invio randomico di immagini o altri file multimediali disponibili. 
	Viene prima di tutto verificato che il 'comando' sia uguale a una delle keywords, in seguito viene inizializzata la creazione dinamica del path.
	Una volta creato il path e il numero casuale, il bot procederà a inviare il file multimediale sulla chat.
	
"""
def randomphoto(comando,chat,didascalia):
		parole_chiave = ['Gordon','gordon','brando','Brando','Simba','simba','Fobio','fobio',
				 'John','john','Guglyee','guglyee','Maury','maury','milano','Milano',
				 'Propic','propic','Odst Propic','odst propic','Myhalomeme','myhalomeme',
				 'Halomeme','halomeme','Meme','meme','Serena','serena','Giona','giona',
				 'Raiden','raiden','Canuzzi','canuzzi']
		dammi = ['Dammi','dammi']
		#comando = str(comando)
		for i in range(len(parole_chiave)):
			if comando.startswith(dammi[0]) or comando.startswith(dammi[1]):
				comando = comando[6:] + " "
				if comando.startswith(parole_chiave[i]) or comando.startswith(parole_chiave[i]):
					comando = parole_chiave[i]
					comando = comando.title()
					numcasuale = random.randint(1,32)
					while True:
						pic = comando + str(numcasuale) + ".jpg"
						path = res + "randomphotos/" + comando + "/" + pic
						try:	
							f = open(path,'rb')
							bot.sendPhoto(chat,f,caption=didascalia)
							print '>>>>Inviata correttamente la foto richiesta<<<< ==> ' + comando
							return
						except:
							numcasuale = random.randint(1,27)
							#numcasuale -= numcas
							#if numcasuale <= 0:
							#numcasuale = random.randint(1,5)
				else:
					continue
			else:
				if comando.startswith(parole_chiave[i]) or comando.startswith(parole_chiave[i]):
					comando = parole_chiave[i]
					comando = comando.title()
					numcasuale = random.randint(1,350)
					while True:
						if comando[0:4] == 'Odst':
							pic = 'PropicODST' + str(numcasuale) + '.jpg'
							path = res + 'randomphotos/Propic/PropicODST/' + pic
						else:
							pic = comando + str(numcasuale) + ".jpg"
							path = res + "randomphotos/" + comando + "/" + pic
						try:
							f = open(path,'rb')
							bot.sendPhoto(chat,f,caption=didascalia)
							print '>>>>Inviata correttamente la foto richiesta<<<< ==> ' + comando
							return
						except:
							numcasuale = random.randint(1,27)
							#numcasuale -= numcas
							#if numcasuale <= 0:
								#numcasuale = random.randint(1,5)

"""
	La seguente funzione serve per salvare in automatico file di tipo immagine dalla chat di telegram direttamente nella cartella desiderata.
	In questo modo è possibile usare la funzione randomphoto per poter richiamare tali immagini senza il bisogno di ulteriore codice.
	
	Parametri: id della relativa chat, il file id dell'immagine da salvare, id del messaggio contenente l'immagine e il nome della cartella in cui salvare.

	Viene controllata quale cartella è stata scelta e viene dunque aperto il file contatore corrispondente, 
	viene creato il path in modo dinamico dove verrà salvato il file, dopo viene incrementato il contatore e sovrascritto il file.
	Infine il file viene salvato e viene restituito un messaggio dell'operazione andata a buon fine.
"""
def saveFile(chat,file_id,mss_id,folder):
	folders = ['Halomeme','Meme']
	if folder == folders[0]:
		file_cont = open("Files/saveMeme/cont_halomeme.txt",'r')
		contatore = int(file_cont.read())
		file_cont.close()
		path = res + "randomphotos/" + folder + "/" + folder + str(contatore) + ".jpg"
		bot.download_file(file_id,path)
		bot.sendMessage(chat,"Meme salvato con successo!",reply_to_message_id = mss_id)
		print '>>>>Meme salvato con successo nella cartella ' + folder + '<<<<'
		file_cont = open("Files/saveMeme/cont_halomeme.txt",'w')
		contatore = contatore + 1
		file_cont.write(str(contatore))
		file_cont.close()
	else:
		file_cont = open("Files/saveMeme/cont_meme.txt",'r')
		contatore = int(file_cont.read())
		file_cont.close()
		path = res + "randomphotos/" + folder + "/" + folder + str(contatore) + ".jpg"
		bot.download_file(file_id,path)
		bot.sendMessage(chat,"Meme salvato con successo!",reply_to_message_id = mss_id)
		print '>>>>Meme salvato con successo nella cartella ' + folder + '<<<<'
		file_cont = open("Files/saveMeme/cont_meme.txt",'w')
		contatore = contatore + 1
		file_cont.write(str(contatore))
		file_cont.close()
	return
