#!/usr/bin/python
# -*- coding: latin-1 -*-
import os, sys

import time
import telepot
import telebot
import commands
import random
import traceback
import time
from datetime import date 
import datetime
import operator
from utils.config import load_config, get_config
#from telegram.ext.dispatcher import run_async
from System import *
from GameSocial import *
from GroupHelper import *
from FileManagement import *
from MessageManagement import *
from Response import *
bot = None
ris = 0


def init_bot():
	bot_code = get_config('robbot', 'code')
	if not bot_code:
		raise Exception('cannot find bot code')

	global bot
	bot = telepot.Bot(bot_code)

#@run_async	
def handle(msg):
	save = msg['chat']['id']
	chat_id = msg['chat']['id']
	msg_id = telepot.glance(msg, long=True)
	mss_id = msg['message_id']
	try:
		reply_mss_id = msg['reply_to_message']['message_id']
	except:
		print '>>>>id messaggio risposta non acquisito ==> Messaggio singolo<<<<'
	mess = msg['from']['id']
	name = msg["from"]["first_name"]
	date = msg['date']
	lista_formati = ['text','photo','audio','document','sticker']
	lista = 0
	command = ''
	fileID = formati(lista_formati,msg)
	if fileID != '':
		command = 'file multimediale'
	else:
		command = msg['text']
	try:
		username = msg['from']['username']
		username = '@' + username
	except:
		print '>>>>Utente senza username<<<<'
		username = 'Non impostato.'
	try:
		nome_chat = msg['chat']['title']
	except:
		if command[0] == '@':
			print '>>>>Error tag<<<< ==>Trying to tag outside a group'
			if mess != 96000757:
				bot.sendMessage(chat_id,'Non puoi taggare un utente al di fuori di un gruppo.')
			else:
				bot.sendMessage(chat_id,'Mio creatore, lo sa bene che non è possibile taggare un utente al di fuori di un gruppo',reply_to_message_id = mss_id)
	###LOGGER PER TRACCIARE I MESSAGGI*-/+*-/+*-/+*-/+-*+/-*+/-+*-/+*-/+*-/+*-/+*-/+*-/+*-/+*-/+*-/+*-/+*-/+-*+/-*+/-*+/-*+-/+*-/+*-/+*-/+-*+/-*+/-*+/-+*-/+*-/
	
	logger(chat_id,name,username,mess,command)
	
	###CHECK PERIODICO SULLE CHAT
	
	check_group_chat(chat_id)
	###Lista chat_gruppi usata successivamente
	chat_gruppi = []
	file_gruppi = open('chat_gruppi.txt','r')
	while 1:
		chat = str(file_gruppi.readline())
		if chat == '':
			break
		chat = chat.replace('\n','')
		chat_gruppi.append(chat)
	file_gruppi.close()
	numchat = len(chat_gruppi)
	
	###SILENZIO_FUNCTION/-*/*-/-/-/*-/-/-*/-/--/*-/*-/*/-/*-/-*/-/*/-*/-/*/-/*-/*-/*-/-*/-*/-*/-/-/-*/-/-//--/-/-/-/-/-/-/-//-/-/-*/-/*-/*-/*/-/*-/*-/*/-*/-/*
	file_check_date = open('Files/Date/check_date.txt','r')
	check_date = file_check_date.read()
	file_check_date.close()
	
	if check_date != "no":
		silenzio()
	
	####controllo sulla data/*/*+/-*+/-*+/-*+/-/*-/*/+-*++/*+/*+/-*+/-*+/-+*-/+*-/+*-/+*-/+*-/+*-/+*-/+*-/+*-/+*-/+-*+/-*+/-*+/-*+/-+*-/+*-/+*-/+*-/+*-/+*-/+*-/
	if date+1*60 < time.time() and mess != chat_id and command != 'file multimediale':
		s = bot.getChatMember(chat_id,96000757)
		if s['status'] == 'left' or s['status'] == 'kicked':
			nome_file = "Files/database_chat_esterne/database_chat_" + str(chat_id) + ".txt"
 			save_chat = open(nome_file,'a')
 			stringa = "ID CHAT: " + str(chat_id) + 'NOME CHAT: '+ nome_chat + ' | ID UTENTE: ' + str(mess) +' | NOME: '+ name+ ' | Nickname: '+ username  + '\nContenuto del messaggio: \n' + command + '\n________________________________________________________________________________________________\n\n'
 			try:
 				save_chat.write(stringa)
 			except:
 				print '>>>>Chat name is unicode type<<<< ==> Saved message from '+ str(chat_id)
 				nome_file = "Files/database_chat_esterne/database_chat_" + str(chat_id) + ".txt"
 				save_chat = open(nome_file,'a')
 				save_chat.write(stringa.encode('utf-8'))
 			save_chat.close()
 			print '>>>>Salvataggio messaggio chat_esterna effettuato<<<<'
 			command = ''
		else:
			command = ''
	
	###GESTIONE UTENTI CON ERRORI USERNAME###
	if mess == 387883829:
		return
	
	###GESTIONE UTENTI BANNATI //da rifare con file o database e comando per ban e unban
	
	"""if mess == 209391499:#32143395 or mess == 31210575:
		numcasuale = random.randint(1,50)
		if numcasuale == 45:
			bot.sendMessage(mess,"Non puoi usare i miei comandi, sei stato bannato a tempo indeterminato dalle mie funzioni")
		return"""
	###CALCOLATRICE
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	lung = len(command)
	operandi = []
	simboli = []
	if command.startswith('Calcola') or command.startswith('calcola'):
		command = command[8:lung]
		operandi = command.split(" ")
		numope = len(operandi)
		for i in range(numope):
			try:
				simbolo = str(operandi[i])
				simboli.append(simbolo)
				operandi.remove(operandi[i])
				numope = len(operandi)
			except:
				print ''
		try:
			if "!" in command:
				command = command.replace('!',"")
				risultato = fattoriale(int(command))
				bot.sendMessage(chat_id,command + "! = " + str(risultato),reply_to_message_id=mss_id)
			else:
				calcolare(chat_id,mss_id,simboli,operandi,command)
		except:
			for i in range(3):
				numcasuale = random.randint(1,3)
				if numcasuale == 1:
					bot.sendMessage(chat_id,"operazione non trovata.",reply_to_message_id = mss_id)
				if numcasuale == 2:
					bot.sendMessage(chat_id,"Errore di input, non posso farci nulla.",reply_to_message_id = mss_id)
				if numcasuale == 3:
					bot.sendMessage(chat_id,"Non posso calcolare tale espressione.",reply_to_message_id = mss_id)
	###FUNZIONE SPEGNI E ACCENDI
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	
	in_file = open('Files/Date/DataMessaggio.txt','r')
	dateOld = int(in_file.read())
	in_file.close()
	
	### check_date viene estratto dal file check_date.txt all'inizio prima di eseguire la funzione silenzio
	membro = bot.getChatMember(chat_id,mess)
	if date - dateOld >= 1 and check_date == 'si':
		if command == '/avvio' or command == '/avvio@KEEEEKBot':
			if mess != 96000757:
				if chat_id != -1001048392729:
					bot.sendMessage(chat_id,'Non sei autorizzato ad avviarmi. Solo il mio creatore può farlo.',reply_to_message_id = mss_id)
				else:
					bot.sendMessage(chat_id,'Non sei autorizzato ad avviarmi. Solo il mio creatore può farlo.'.upper(),reply_to_message_id = mss_id)
			else:
				bot.sendMessage(chat_id,'Sono di nuovo attivo. Da questo momento riprenderò il mio lavoro.')
				out_file = open('Files/Date/check_date.txt','w')
				check_date = 'no'
				out_file.write(check_date)
				out_file.close()
		else:
			command = ''
	elif check_date == 'no' and command == '/avvio@KEEEEKBot':
		if mess != 96000757:
			if chat_id != -1001048392729:
				bot.sendMessage(chat_id,'Non hai il permesso di avviarmi. Comunque sono già attivo.')
			else:
				bot.sendMessage(chat_id,'Non hai il permesso di avviarmi. Comunque sono già attivo.'.upper())
		else:
			bot.sendMessage(chat_id,'Sono già attivo e operativo, mio creatore.')
	elif command == '/spegnimento' or command == '/spegnimento@KEEEEKBot':
		if mess != 96000757:
			if chat_id != -1001048392729:
				bot.sendMessage(chat_id,'Non sei autorizzato a spegnermi. Solo il mio creatore può farlo.',reply_to_message_id = mss_id)
			else:
				bot.sendMessage(chat_id,'Non sei autorizzato a spegnermi. Solo il mio creatore può farlo.'.upper(),reply_to_message_id = mss_id)
		else:
			bot.sendMessage(chat_id,'Da questo momento continuerò a leggere i messaggi in modalità passiva, fino ad un nuovo avvio.')
			out_file = open('Files/Date/check_date.txt','w')
			check_date = 'si'
			out_file.write(check_date)
			out_file.close()
			out_file = open('Files/Date/DataMessaggio.txt','w')
			date = str(date)
			out_file.write(date)
			out_file.close()
	#STATISTICHE MESSAGGI LETTI
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	in_file = open('Files/Messaggi/conteggio.txt','r')
	contenuto =	in_file.read()
	in_file.close()
	contM = int(contenuto)
	contM = contM + 1
	out_file = open('Files/Messaggi/conteggio.txt','w')
	contM = str(contM)
	out_file.write(contM)
	#STATISTICHE NUMERO UTENTI RICONOSCIUTI + NOMI E NICKNAME
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	in_file = open('IDUtenti.txt','r')
	file_nomi = open('nomi.txt','r')
	file_username = open('username.txt','r')
	Nomi = []
	Nickname = []
	Utenti = []
	utente = 1
	##WHILE PER LEGGERE GLI ID UTENTI
	while 1:
		utente = int(in_file.readline())
		if utente == 0:
			break
		Utenti.append(utente)
	in_file.close()
	##WHILE PER LEGGERE I NOMI E I NICKNAME
	while 1:
		nome = str(file_nomi.readline())
		if nome == '':
			break
		Nomi.append(nome)
		nick = str(file_username.readline())
		Nickname.append(nick)
	file_nomi.close()
	file_username.close()
	##VERIFICA ED EVENTUALMENTE AGGIUNTA DI UN NUOVO UTENTE
	NumUtenti = len(Utenti)
	h = 0
	for i in range(NumUtenti):
		if username == '@normaloide' or username == 'ERRORE':
			break
		if mess == Utenti[i] and (name+'\n') == Nomi[i] and (username+'\n') == Nickname[i]:
			break
		elif ((name+'\n') != Nomi[i] or (username+'\n') != Nickname[i]) and mess == Utenti[i]:
			Nomi[i] = name + '\n'
			Nickname[i] = username + '\n'
			file_nomi = open('nomi.txt','w')
			file_username = open('username.txt','w')
			NumNomi = len(Nomi)
			try:
				for i in range(NumUtenti):
					z = i
					file_nomi.write(Nomi[i])
					file_username.write(Nickname[i])
				file_nomi.write('nome non ancora ottenuto\n')
				file_nomi.close()
				file_username.write('username non ancora ottenuto\n')
				file_username.close()
				print '>>>>Salvataggio nuovo nome e nickname<<<<'
			except:
				print '>>>>error save name<<<<'
				file_nomi.write(Nomi[i].encode('utf-8'))
				file_username.write(Nickname[z])
				z = z + 1
				while z <= NumUtenti:
					file_nomi.write(Nomi[z])
					file_username.write(Nickname[z])
					z = z + 1
				file_nomi.write('nome non ancora ottenuto\n')
				file_nomi.close()
				file_username.write('username non ancora ottenuto\n')
				file_username.close()
			break
		elif i == len(Utenti) - 1:
			Utenti.append(mess)
			out_file = open('IDUtenti.txt','w')
			for i in range(NumUtenti+1):
				utente = str(Utenti[i])
				out_file.write(utente)
				out_file.write('\n')
			out_file.write('0')
			out_file.close()
			print '>>>>Salvataggio nuovo ID utente<<<<'
			h = 1
			Nomi[NumUtenti] = name + '\n'
			if username == 'username':
				Nickname[NumUtenti] = 'Non impostato.\n'
			else:
				Nickname[NumUtenti] = username + '\n'
			file_nomi = open('nomi.txt','w')
			file_username = open('username.txt','w')
			NumNomi = len(Nomi)
			try:
				for i in range(NumNomi):
					z = i
					file_nomi.write(Nomi[i])
					file_username.write(Nickname[i])
				file_nomi.write('nome non ancora ottenuto\n')
				file_nomi.close()
				file_username.write('username non ancora ottenuto\n')
				file_username.close()
				print '>>>>Salvataggio nuovo nome e nickname<<<<'
			except:
				print '>>>>error save name<<<<'
				file_nomi.write(Nomi[i].encode('utf-8'))
				file_username.write(Nickname[z])
				z = z + 1
				h = 1
				while z <= NumNomi:
					file_nomi.write(Nomi[z])
					file_username.write(Nickname[z])
					z = z + 1
				file_nomi.write('nome non ancora ottenuto\n')
				file_nomi.close()
				file_username.write('username non ancora ottenuto\n')
				file_username.close()
			if date - dateOld >= 1000 and check_date == 'no' or (date+1*60 < time.time()):
				if mess == chat_id:
					bot.sendMessage(chat_id,'Nuovo utente salvato! Benvenuto.',reply_to_message_id = mss_id)
			break
			
	#STATISTICHE NUMERO MESSAGGI PER UTENTE
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	in_file = open('Files/Messaggi/conteggioMessaggiUtente.txt','r')
	file_GG = open('Files/Messaggi/conteggioMessGG.txt','r')
	file_SET= open('Files/Messaggi/conteggioMessSett.txt','r')
	file_punti = open('Files/QuickMath/punti_quickMath.txt','r')
	punteggio = []
	contSET= []
	contGG = []
	contAll = []
	NumUtenti = len(Utenti)
	while 1:
		n = int(in_file.readline())
		m = int(file_GG.readline())
		s = int(file_SET.readline())
		p = int(file_punti.readline())
		if p == 1234567:
			break
		contAll.append(n)
		contGG.append(m)
		contSET.append(s)
		punteggio.append(p)
	in_file.close()
	file_GG.close()
	file_SET.close()
	file_punti.close()
	for i in range(NumUtenti):
		if mess == Utenti[i]:
			contAll[i] = contAll[i] + 1
			contGG[i] = contGG[i] + 1
			contSET[i] = contSET[i] + 1
			break
		else:
			continue
	Numcont = len(contAll)
	out_file = open('Files/Messaggi/conteggioMessaggiUtente.txt','w')
	file_GG = open('Files/Messaggi/conteggioMessGG.txt','w')
	file_SET= open('Files/Messaggi/conteggioMessSett.txt','w')
	file_punti = open('Files/QuickMath/punti_quickMath.txt','w')
	for i in range(Numcont):
		n = str(contAll[i])
		m = str(contGG[i])
		s = str(contSET[i])
		p = str(punteggio[i])
		file_punti.write(p)
		file_punti.write("\n")
		out_file.write(n)
		out_file.write('\n')
		file_GG.write(m)
		file_GG.write('\n')
		file_SET.write(s)
		file_SET.write('\n')
	if h == 1:
		out_file.write('0\n')
		file_GG.write('0\n')
		file_SET.write('0\n')
		file_punti.write('0\n')
	file_GG.write('1234567')
	out_file.write('1234567')
	file_SET.write('1234567')
	file_punti.write('1234567')
	out_file.close()
	file_GG.close()
	file_SET.close()
	file_punti.close()
	#STATISTICA GIORNALIERA
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	data_corrente = int(date)
	file_date = open('Files/Date/data_giorno.txt','r')
	file_gg = open('Files/Messaggi/conteggioGiornaliero.txt','r')
	contG = int(file_gg.read())
	data_vecchia = int(file_date.read())
	file_date.close()
	file_gg.close()
	if data_corrente - data_vecchia >= 86400:
		numchat = len(chat_gruppi)
		for i in range(numchat):
			chat = chat_gruppi[i]
			try:
				if bot.getChat(chat):
					print '>>>>Sono ancora in questa chat<<<< ==>' + str(chat)
			except:
				chat_gruppi.remove(chat)
				print '>>>>chat rimossa<<<< ==> causa rimozione del bot'
				file_gruppi = open('chat_gruppi.txt','w')
				for i in range(numchat):
					file_gruppi.write(chat)
				file_gruppi.close()
			file_GG = open('Files/Messaggi/conteggioMessGG.txt','w')
			for i in range(len(contGG)):
				contGG[i] = "0"
				file_GG.write(contGG[i])
				file_GG.write('\n')
			file_GG.write("1234567")
			file_GG.close()
		file_date = open('Files/Date/data_giorno.txt','w')
		file_date.write(str(data_corrente))
		file_date.close()
		today = time.strftime("%d/%m/%Y")
		file_database = open('Files/Messaggi/MessaggiGiornalieri.txt','a')
		data = str(today) + ': ' + str(contG) + ' messaggi\n'
		file_database.write(data)
		file_database.close()
		contG = 0
		file_gg = open('Files/Messaggi/conteggioGiornaliero.txt','w')
		file_gg.write(str(contG))
		file_gg.close()
	else:
		contG = contG + 1
		file_gg = open('Files/Messaggi/conteggioGiornaliero.txt','w')
		file_gg.write(str(contG))
		file_gg.close()
		
	
	#STATISTICA SETTIMANALE
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	file_dateS = open('Files/Date/data_settimana.txt','r')
	file_sett = open('Files/Messaggi/conteggioSettimanale.txt','r')
	contS = int(file_sett.read())
	data_vecchia = int(file_dateS.read())
	file_sett.close()
	file_dateS.close()
	if data_corrente - data_vecchia >= 604800:
		numchat = len(chat_gruppi)
		file_SET = open('Files/Messaggi/conteggioMessSett.txt','w')
		for i in range(len(contSET)):
			contSET[i] = "0"
			file_SET.write(contSET[i])
			file_SET.write('\n')
		file_SET.write("1234567")
		file_SET.close()
		print '>>>>Inviata la statistica settimanale alla chat<<<< ==>' + str(chat)
		file_dateS = open('Files/Date/data_settimana.txt','w')
		file_dateS.write(str(data_corrente))
		file_dateS.close()
		today = time.strftime("%d/%m/%Y")
		file_database = open('Files/Messaggi/MessaggiSettimanali.txt','a')
		data = str(today) + ': ' + str(contS) + ' messaggi\n'
		file_database.write(data)
		file_database.close()
		contS = 0
		file_sett = open('Files/Messaggi/conteggioSettimanale.txt','w')
		file_sett.write(str(contG))
		file_sett.close()
	else:
		contS = contS + 1
		file_sett = open('Files/Messaggi/conteggioSettimanale.txt','w')
		file_sett.write(str(contS))
		file_sett.close()
	#CALCOLO DELL'UTENTE CON PIÙ MESSAGGI INVIATI
	massimo = 0
	for i in range(len(contAll)):
		if int(contAll[i]) > massimo:
			massimo = contAll[i]
			NomeMax = Nomi[i]
		else:
			continue
	#VISUALIZZAZIONE STATISTICHE PER GRUPPO
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	stringa = ""
	stringaquickmath = ""
	if (command == 'Stat gruppo' or command == 'STAT GRUPPO' or command == '/statgruppo' or command == '/statgruppo@KEEEEKBot') and mess != chat_id:
		if chat_id != -1001048392729:
			bot.sendMessage(chat_id,"Si prega di attendere...")
		else:
			bot.sendMessage(chat_id,"Si prega di attendere...".upper())
		gruppoMess = []
		gruppoNome = []
		points = []
		for i in range(len(Utenti)):
			try:
				if contAll[i] > 10:
					s = bot.getChatMember(chat_id,Utenti[i])
					if s['status'] != 'left' and s['status'] != 'kicked':
						gruppoMess.append(contAll[i])
						gruppoNome.append(Nomi[i].replace("\n",""))
						points.append(punteggio[i])
			except:
				print '>>>>Utente non trovato su questa chat.<<<< ==>' + str(chat_id)
		for passesLeft in xrange(len(gruppoMess)-1, 0, -1):
			for i in xrange(passesLeft):
				if i == len(gruppoMess) - 1:
					break
				if gruppoMess[i+1] > gruppoMess[i]:
					saveMess = gruppoMess[i]
					gruppoMess[i] = gruppoMess[i+1]
					gruppoMess[i+1] = saveMess
					saveNome = gruppoNome[i]
					gruppoNome[i] = gruppoNome[i+1]
					gruppoNome[i+1] = saveNome
		for i in range(len(gruppoMess)):
			stringa = stringa + str(i+1) + ". " + str(gruppoNome[i]) + ": " + str(gruppoMess[i]) + " messaggi" + "\n"
			stringaquickmath = stringaquickmath + str(i+1) + ". " + str(gruppoNome[i]) + ": " + str(points[i]) + " punti" + "\n"
		bot.sendMessage(chat_id,"Classifica attività utenti:\n__________________________\n"+ stringa)
	###CLASSIFICA PUNTI QUICKMATH GRUPPO
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	if command == '/scorequickmath@KEEEEKBot' and mess != chat_id:
		if chat_id != -1001048392729:
			bot.sendMessage(chat_id,"Si prega di attendere...")
		else:
			bot.sendMessage(chat_id,"Si prega di attendere...".upper())
		gruppoNome = []
		points = []
		for i in range(len(contAll)):
			try:
				if punteggio[i] != 0:
					if bot.getChatMember(chat_id,Utenti[i]):
						s = bot.getChatMember(chat_id,Utenti[i])
						if s['status'] != 'left' and s['status'] != 'kicked':
							gruppoNome.append(Nomi[i].replace("\n",""))
							points.append(punteggio[i])
			except:
				print '>>>>Utente non trovato su questa chat.<<<< ==>' + str(chat_id)
		for passesLeft in xrange(len(gruppoNome)-1, 0, -1):
			for i in xrange(passesLeft):
				if i == len(gruppoNome) - 1:
					break
				if points[i+1] > points[i]:
					savePoints = points[i]
					points[i] =points[i+1]
					points[i+1] = savePoints
					saveNome = gruppoNome[i]
					gruppoNome[i] = gruppoNome[i+1]
					gruppoNome[i+1] = saveNome
		for i in range(len(gruppoNome)):
			if points[i] == 172:
				stringaquickmath = stringaquickmath + str(i+1) + ". " + str(gruppoNome[i]) + ": " + str(points[i]) + " punti(BANNATO per cheat)" + "\n"
			else:
				stringaquickmath = stringaquickmath + str(i+1) + ". " + str(gruppoNome[i]) + ": " + str(points[i]) + " punti" + "\n"
		gioc = 0
		for i in range(len(punteggio)):
			if punteggio[i] != 0:
				gioc = gioc + 1
		stringaquickmath = stringaquickmath + "\n\nAttualmente sono stati registrati complessivamente" + str(gioc) + " giocatori."
		bot.sendMessage(chat_id,"Classifica punteggio quickMath in questo gruppo:\n_________________________\n" + stringaquickmath)
	if (command == '/scorequickmath' and mess == chat_id) or command == '/myscorequickmath@KEEEEKBot' or command == '/myscorequickmath':
		gioc = 0
		score = 0
		for i in range(len(Utenti)):
			if punteggio[i] != 0:
				gioc = gioc + 1
			if mess == Utenti[i] and score == 0:
				score = punteggio[i]
		if mess != 96000757:
			bot.sendMessage(chat_id,"{N}, il tuo punteggio è di {P} punti.\n\nAttualmente sono registrati complessivamente {G} giocatori".format(N = name,P = score,G = gioc))
		else:
			bot.sendMessage(chat_id,"Mio creatore, il tuo punteggio è di {P} punti. Con lei posso essere più morbido sulle domande se vuole...\n\nAttualmente sono registrati {G} giocatori".format(P = score,G = gioc))
	if command == '/globalscorequickmath@KEEEEKBot' or command == '/globalscorequickmath':
		stringaquickmath = "Classifica globale quickMath:\n_________________________\n"
		point = 0
		nome = ""
		for passesLeft in xrange(len(punteggio)-1, 0, -1):
			for i in xrange(passesLeft):
				if i == len(punteggio) - 1:
					break
				if punteggio[i+1] > punteggio[i]:
					point = punteggio[i]
					punteggio[i] = punteggio[i+1]
					punteggio[i+1] = point
					nome = Nomi[i]
					Nomi[i] = Nomi[i+1]
					Nomi[i+1] = nome
		for i in range(len(Utenti)):
			if punteggio[i] != 0:
				if punteggio[i] == 172:
					stringaquickmath = stringaquickmath + str(i+1) + ". " + str(Nomi[i].replace("\n","")) + ": " + str(punteggio[i]) + " punti(BANNATO per cheat)" + "\n"
				else:
					stringaquickmath = stringaquickmath + str(i+1) + ". " + str(Nomi[i].replace("\n","")) + ": " + str(punteggio[i]) + " punti" + "\n"
		bot.sendMessage(chat_id,stringaquickmath)
	###REGOLE QUICKMATH
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	if command == '/rulesquickmath@KEEEEKBot' or command == '/rulesquickmath':
		bot.sendMessage(chat_id,"In quickMath dovrai rispondere a semplici quesiti matematici in meno di 10 secondi.\n\n Ad ogni risposta esatta ricevi un punto, il punteggio finale è il numero di risposte esatte consecutive. Buona fortuna.\n\n Per giocare scrivi <quickMath> senza i <> oppure usa il comando /quickmath",reply_to_message_id = mss_id)
	###STATISTICA PER GRUPPO GIORNALIERA
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	elif (command == 'Stat gruppo GG' or command == 'STAT GRUPPO GG' or command == '/statgruppogg' or command == '/statgruppogg@KEEEEKBot') and mess != chat_id:
		if chat_id != -1001048392729:
			bot.sendMessage(chat_id,"Si prega di attendere...")
		else:
			bot.sendMessage(chat_id,"Si prega di attendere...".upper())
		gruppoMessGG = []
		gruppoNome = []
		for i in range(len(Utenti)):
			try:
				if contGG[i] > 10:
						s = bot.getChatMember(chat_id,Utenti[i])
						if s['status'] != 'left' and s['status'] != 'kicked':
							gruppoMessGG.append(contGG[i])
							gruppoNome.append(Nomi[i].replace("\n",""))
			except:
				print '>>>>Utente non trovato su questa chat.<<<< ==>' + str(chat_id)
		for passesLeft in xrange(len(gruppoMessGG)-1, 0, -1):
			for i in xrange(passesLeft):
				if i == len(gruppoMessGG) - 1:
					break
				if gruppoMessGG[i+1] > gruppoMessGG[i]:
					saveMess = gruppoMessGG[i]
					gruppoMessGG[i] = gruppoMessGG[i+1]
					gruppoMessGG[i+1] = saveMess
					saveNome = gruppoNome[i]
					gruppoNome[i] = gruppoNome[i+1]
					gruppoNome[i+1] = saveNome
		for i in range(len(gruppoMessGG)):
			stringa = stringa + str(i+1) + ". " + str(gruppoNome[i]) + ": " + str(gruppoMessGG[i]) + " messaggi" + "\n"
		bot.sendMessage(chat_id,"Classifica giornaliera attività utenti:\n___________________________________\n"+ stringa)
	elif mess == chat_id and (command == '/statgruppo' or command == '/statgruppogg' or command == 'Stat gruppo' or command == 'Stat gruppo GG'):
		if mess != 96000757:
			bot.sendMessage(chat_id,"Non è possibile utilizzare questo comando al di fuori di una chat di gruppo.\nMi dispiace, cosi mi è stato ordinato.")
		else:
			bot.sendMessage(chat_id,"Mio creatore, lo sa bene che non è possibile utilizzare questo comando al di fuori di una chat di gruppo.")
		
	
	#VISUALIZZAZIONE STATISTICHE
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	if command == 'Stat' or command == '/stat' or command == '/stat@KEEEEKBot':
		if chat_id != -1001048392729:
			bot.sendMessage(chat_id,"Fino ad oggi ho letto complessivamente {M} messaggi! \n\nTotale utenti registrati: {U} \n\nAl momento sono presente in {N} gruppi!\n\nL'utente più attivo con il maggior numeri di messaggi inviati è:\n\n{NM} con un totale di {Z} messaggi".format(M = contM, U = len(Utenti),N = numchat,NM = NomeMax.replace("\n",""),Z = massimo))
		else:
			bot.sendMessage(chat_id,"Fino ad oggi ho letto complessivamente {M} messaggi! \n\nTotale utenti registrati: {U} \n\nAl momento sono presente in {N} gruppi!\n\nL'utente più attivo con il maggior numeri di messaggi inviati è:\n\n{NM} con un totale di {Z} messaggi".upper().format(M = contM, U = len(Utenti),N = numchat,NM = NomeMax.replace("\n",""),Z = massimo))
	if (ricercaStringhe(command,'Stat') and ricercaStringhe(command,'Mia')) or command == '/statmia' or command == '/statmia@KEEEEKBot':
		for i in range(NumUtenti):
			if mess == Utenti[i]:
				if chat_id != -1001048392729:
					bot.sendMessage(chat_id,'Utente: {N}\nUsername: {U}\nFinora hai inviato un totale di {C} messaggi'.format(N = Nomi[i],U = Nickname[i],C = contAll[i]),reply_to_message_id = mss_id)
				else:
					bot.sendMessage(chat_id,'Utente: {N}\nUsername: {U}\nFinora hai inviato un totale di {C} messaggi'.upper().format(N = Nomi[i],U = Nickname[i],C = contAll[i]),reply_to_message_id = mss_id)
				break
			else:
				continue
	
	
	###SARAHA/*/*/*//*/**/*/*/-/*-/*/-/*-/*-/*-/*/-*/-//*-/*-/*/-/*-/*/-*/-*/--/-/*-/-*/*-/-/*-/-//-/-*/-*/-/-*/-*/-/-/-/--//-/-*/-/-*/--/-*/-*/-*/-*/-*
	if command == '/rulesanon' or command == '/rulesanon@KEEEEKBot':
		bot.sendMessage(chat_id,"Con questa funzionalità avrai la possibilità di lasciare un commento in anonimo a un utente in particolare.\n\n Per poter utilizzare tale funzione utilizzare la sintassi illustrata qui di seguito.\n\n/anon @username corpo del messaggio\nEsempio: /anon @RobBot ehy sei proprio un bel bot!",reply_to_message_id = mss_id)
		
	""" Tramite questa funzionalità viene selezionato un id utente e un messaggio e viene chiamata la funzione saraha che invierà tale messaggio all' utente 
		selezionato in anonimo senza lasciare alcuna traccia. """
		
	if command[0:5] == '/anon' and mess == chat_id:
		saraha(mess,command,Utenti,Nickname)
	elif command[0:5] == '/anon' and mess != chat_id:
		if mess != 96000757:
			bot.sendMessage(chat_id,"Non è possibile usare questo comando all'interno di un gruppo. Si prega di digitare in privato.")
		else:	
			bot.sendMessage(chat_id,"Mio creatore, lo sa bene che non è possibile usare questo comando all'interno di un gruppo.")			
	###RICHIAMA MESSAGGIO/*/-/-*/*-*/-//*-/-*/*//-///*-/-/-/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-*/-/-/-/-/-/-/-/--/-*//--/
	if command[0:8] == 'Richiama':
		command = command[9:]
		pickmessagesaved(chat_id,command)		
	###SALVA MESSAGGIO/-*/-*/*/*-//*/*-/*///-*/-*/-*/*-/-*/*-/*-/-/-*/*/-*//-*///-*/////-//-/-/-/-/-/-///-/*/*/*/-/*-/*-/*-/*-/*/-*/-*/-/*-/*-/*-/*-/*-/*
	try:
		if command.startswith('Save') or command.startswith('save'):
			command = command.split(" ")
			if len(command) > 2:
				return
			richiamo = command[1]
			reply_mss_id = msg['reply_to_message']['message_id']
			savemessage(chat_id,reply_mss_id,richiamo)
			return
	except:
		bot.sendMessage(chat_id,"Sintassi errata",reply_to_message_id=mss_id)
		return
	####CANCELLA MESSAGGIO/*-/*-/-*/-/*-/*-*/-/*-/*/-/*-/*-/*/-/*-/*-/*-/*-/*/-/*-/*-/*/-*/-/*-/*/-*/-/*-/*-/*/-/*-/*-/*/-*/-/*-/*-/*/-/*-/*/-*/-/*/-*/-/*/-
	if command.startswith('Cancella') or command.startswith('cancella') and mess == 96000757:
		command = command.split(" ")
		if len(command) > 2:
			return
		richiamo = command[1]
		deleteMessage(chat_id,richiamo)
	###LISTA MESSAGGI SALVATI/-*/-/*-/*-/*/-/*-/*/-*/-/*-/*/-*/-/*-/*/-*/-/*-/*/-*/-/*-/*/-*/-/*-/*/-*/-*/-/*-/*-/*-/*-/*/-*/-/*-/*/-/*-/*/-*-*/-/*-/*/-*/-
	if command == 'getMessaggi':
		listaMessaggiSalvati(chat_id,mss_id)
	###RULESAVEMESSAGE/*/*/*/*/*/*-/-/*/*/*/*//*/*/*-/-/-*/-/-/-*-/*-/*/-*/-/*-/*/-/*-/*/-/*-/*-/*/-/*-/*/-/*-/*-/*/-*/-/*-/*/-*/-/*-/*/-*/-/*-/*/-/*-/-*/-/
	if command == '/rulesavemessage' or command == '/rulesavemessage@KEEEEKBot':
		bot.sendMessage(chat_id,"Istruzioni per il salvataggio di messaggi all'interno di un gruppo.\nRispondere al messaggio desiderato digitando save, seguito da un nome simbolico per poterlo richiamare in seguito.\n\nEsempio: save Pluto\nPer richiamare un messaggio basta digitare Richiama Pluto.\nPer la lista dei messaggi salvati sulla chat corrente, digitare getMessaggi.")
	###MESSAGGI GIORNALIERI
	if command == 'getDataGG' and mess == 96000757:
		file_msg = open('Files/Messaggi/MessaggiGiornalieri.txt','r')
		lista = file_msg.read()
		bot.sendMessage(chat_id,"Ecco i dati sui messaggi giornalieri ottenuti fino ad oggi, mio creatore\n\n" + lista)
		file_msg.close()
	elif command == 'getDataGG' and mess != 96000757:
		bot.sendMessage(chat_id,"Non sei autorizzato per questo comando.",reply_to_message_id=mss_id)
	###MESSAGGI SETTIMANALI
	if command == 'getDataSETT' and mess == 96000757:
		file_msg = open('Files/Messaggi/MessaggiSettimanali.txt','r')
		lista = file_msg.read()
		bot.sendMessage(chat_id,"Ecco i dati sui messaggi settimanali ottenuti fino ad oggi, mio creatore\n\n" + lista)
		file_msg.close()
	elif command == 'getDataSETT' and mess != 96000757:
		bot.sendMessage(chat_id,"Non sei autorizzato per questo comando.",reply_to_message_id=mss_id)
	###SALVA MEMES
	if command == '/rulesavememes' or command == 'rulesavememes@KEEEEKBot':
		bot.sendMessage(chat_id,"Questa funzionalità permette agli utenti di richiamare o salvare memes.\n\nEsistono due tipologie di memes salvabili:\n1)Meme\n2)Halomeme\n\nPer salvare un meme, andare nella mia chat privata e dare un comando come 'salva pic <tipo-meme>', in seguito inviare il meme da salvare.\n\nesempio: salva pic Meme\n>invio immagine\nPer richiamare un meme basta scrivere il tipo di meme come messaggio.\n\nSpero di essere stato chiaro!")
	
	
	#CONTROLLO SUL TRIGGER CHE PERMETTE DI INVIARE UN MEDIA PER SALVARE IL MEME
	file_cont = open('Files/saveMeme/cont_halomeme.txt','r')
	contatore_memes = int(file_cont.read().replace('\n',''))
	file_cont.close()
	file_cont = open('Files/saveMeme/cont_meme.txt','r')
	contatore_memes = contatore_memes + int(file_cont.read().replace('\n',''))
	file_cont.close()
	tipologia_memes = ['Meme','Halomeme']
	file_trigger = open("Files/saveMeme/trigger_save.txt",'r')
	save_meme = file_trigger.read().replace('\n','')
	file_trigger.close()
	if (command[0:9] == 'salva pic' or command[0:9] == 'Salva pic') and save_meme == 'no' and mess == chat_id:
		tipo = command[10:].title()
		tipo = tipo.title()
		if tipo != tipologia_memes[0] and tipo != tipologia_memes[1]:
			bot.sendMessage(chat_id,"Tipo di meme non riconosciuto, si prega di inserire un tipo esistente.\n Le tipologie esistenti al momento sono: \n1) " + tipologia_memes[0] +"\n2) " + tipologia_memes[1] + "\nAl momento sono stati salvati "+ str(contatore_memes) + " memes.",reply_to_message_id=mss_id)
			return
		file_tipo = open("Files/saveMeme/tipo_save.txt",'w')
		file_tipo.write(tipo)
		file_tipo.close()
		file_trigger = open("Files/saveMeme/trigger_save.txt",'w')
		file_trigger.write("ok")
		file_trigger.close()
		bot.sendMessage(chat_id,"Invia pure il meme da salvare",reply_to_message_id=mss_id)
	elif command == 'file multimediale' and save_meme == 'ok' and mess == chat_id:
		file_tipo = open("Files/saveMeme/tipo_save.txt",'r')
		tipo = file_tipo.read().replace('\n','')
		file_tipo.close()
		saveFile(chat_id,fileID,mss_id,tipo)
		file_trigger = open("Files/saveMeme/trigger_save.txt",'w')
		file_trigger.write("no")
		file_trigger.close()
	elif command[0:10] == 'salva meme' and save_meme == 'no' and mess != chat_id:
		if mess != 96000757:
			bot.sendMessage(chat_id,"Se vuoi salvare un meme, devi dirmelo in privato. Non è possibile all'interno di un gruppo.\n\nLe tipologie esistenti al momento sono: \n1) " + tipologia_memes[0] +"\n2) " + tipologia_memes[1] + "\nAl momento sono stati salvati "+ str(contatore_memes) + " memes.",reply_to_message_id=mss_id)
		else:
			bot.sendMessage(chat_id,"Mio creatore, lo sa bene che non è possibile salvare meme all'interno di un gruppo.",reply_to_message_id=mss_id)
	
	###PIN_MESSAGE
	if len(command) == 12:
		command = command.title()
	if command[0:12] == 'Robbot Pinna':
		pinMessage(mess,chat_id,reply_mss_id)
		return
	###QUICKMATH
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	file_math = open('Files/QuickMath/risultato.txt','r')
	ris = int(file_math.read())
	file_math.close()
	file_math = open('Files/QuickMath/giocatore.txt','r')
	giocatore = int(file_math.read())
	file_math.close()
	file_math = open('Files/QuickMath/tempo.txt','r')
	
	###Recupero il tempo vecchio e gli aggiungo i 10 secondi disponibili per la risposta
	
	tempo_split = file_math.read()
	tempo_split = tempo_split.split(":")
	tempo_split[2] = int(tempo_split[2]) + 10
	if tempo_split[2] >= 60:
		tempo_split[2] = tempo_split[2] - 60
	tempo = datetime.time(int(tempo_split[0]),int(tempo_split[1]),int(tempo_split[2]))
	file_math.close()
	
	###Recupero il tempo corrente
	
	
	tempo_split = time.strftime("%H:%M:%S")
	tempo_split = tempo_split.split(":")
	tempo_attuale = datetime.time(int(tempo_split[0]),int(tempo_split[1]),int(tempo_split[2]))
	punti = 0
	calcola = 0
	
	###Effettuo il confronto tra il tempo in cui è arrivata la domanda e il tempo in cui è stata digitata la risposta
	
	if tempo_attuale > tempo and ris != 999999:
			bot.sendMessage(giocatore,"Mi dispiace, tempo scaduto.")
			ris = 999999
			file_math = open('Files/QuickMath/risultato.txt','w')
			file_math.write(str(ris))
			file_math.close()
			calcola = 1
	if ris != 999999 and mess == giocatore:
		while tempo > tempo_attuale:
			if int(command) == ris:
				bot.sendMessage(chat_id,"Esatto!")
				ris = 1
				file_temp = open('Files/QuickMath/punti_temp.txt','r')
				temp = int(file_temp.read())
				file_temp.close()
				punti = temp + 1
				file_temp = open('Files/QuickMath/punti_temp.txt','w')
				file_temp.write(str(punti))
				file_temp.close()
				command = 'quickMath'
				file_math = open('Files/QuickMath/risultato.txt','w')
				file_math.write(str(ris))
				file_math.close()
				break
			else:
				bot.sendMessage(chat_id,"Hai perso!")
				ris = 999999
				file_math = open('Files/QuickMath/risultato.txt','w')
				file_math.write(str(ris))
				file_math.close()
				calcola = 1
				break
			continue
		if tempo_attuale > tempo and mess == chat_id:
			bot.sendMessage(chat_id,"Mi dispiace, tempo scaduto.")
			ris = 999999
			file_math = open('Files/QuickMath/risultato.txt','w')
			file_math.write(str(ris))
			file_math.close()
			calcola = 1
	if calcola == 1:
		for i in range(len(punteggio)):
			if giocatore == Utenti[i]:
				file_temp = open('Files/QuickMath/punti_temp.txt','r')
				temp = int(file_temp.read())
				file_temp.close()
				if temp > punteggio[i]:
					bot.sendMessage(chat_id,"Hai stabilito un nuovo record!")
					punteggio[i] = temp + 1
					file_punti = open('Files/QuickMath/punti_quickMath.txt','w')
					for i in range(len(punteggio)):
						file_punti.write(str(punteggio[i]))
						file_punti.write('\n')
					file_punti.write("1234567")
					file_punti.close()
					file_temp = open('Files/QuickMath/punti_temp.txt','w')
					file_temp.write(str(0))
					file_temp.close()
					break
				else:
					file_temp = open('Files/QuickMath/punti_temp.txt','w')
					file_temp.write(str(0))
					file_temp.close()
					break
			else:
				continue
		
	if (command == 'quickMath' or command == '/quickmath@KEEEEKBot' or command == '/quickmath') and mess == chat_id:
		if ris != 999999 and mess != giocatore:
			bot.sendMessage(chat_id,"Troppi giocatori al momento, si prega di riprovare tra poco")
			#file_math = open('giocatore.txt','w')
			#file_math.write(str(giocatore))
			#file_math.close()
			return
		file_math = open('Files/QuickMath/giocatore.txt','w')
		file_math.write(str(mess))
		file_math.close()
		if ris == 999999:
			if giocatore == 25571068:
				bot.sendMessage(giocatore,"Non è possibile giocare a quickMath. Sei stato messo in black list per cheating.")
				return
			try:
				bot.sendMessage(chat_id, "Giocatore attivo {N}.".format(N = name))
			except:
				bot.sendMessage(chat_id,"Giocatore attivo")
		ope = 0
		ope2= 0
		opx = ""
		while 1:
			ris = 0
			ope = random.randint(0,25)
			ope2= random.randint(0,10)
			opx = { "+": operator.add, "-": operator.sub, "*": operator.mul}
			operatori = ["+","-","*","^"]
			simb = random.randint(0,3)
			Special = random.randint(1,10)
			if Special % 2 == 0:
				ope3 = random.randint(0,10)
				simboli = ["+","-","*"]
				simbolo = random.randint(0,2)
				simbolo2 = random.randint(0,2)
				ops = { "+": operator.add, "-": operator.sub, "*": operator.mul }
				risul = ops[simboli[simbolo]](ope,ope2)
				ris = ops[simboli[simbolo2]](risul,ope3)
				file_math = open('Files/QuickMath/risultato.txt','w')
				file_math.write(str(ris))
				file_math.close()
				bot.sendMessage(chat_id,"{N} {S} {M} = x\n\nx {S2} {Z} = ???".format(N = ope,S = simboli[simbolo],M = ope2,S2 = simboli[simbolo2],Z = ope3))
				file_math = open('Files/QuickMath/tempo.txt','w')
				file_math.write(str(tempo_attuale))
				file_math.close()
				file_math = open('Files/QuickMath/giocatore.txt','w')
				file_math.write(str(mess))
				file_math.close()
				break
			if simb == 3:
				ope = random.randint(0,9)
				ope2= random.randint(0,3)
				opx = "^"
				ris = int(ope) ** int(ope2)
				file_math = open('Files/QuickMath/risultato.txt','w')
				file_math.write(str(ris))
				file_math.close()
				bot.sendMessage(chat_id,"{N}^{M} = ???".format(N = ope,M = ope2))
				file_math = open('Files/QuickMath/tempo.txt','w')
				file_math.write(str(tempo_attuale))
				file_math.close()
				file_math = open('Files/QuickMath/giocatore.txt','w')
				file_math.write(str(mess))
				file_math.close()
				break
			else:
				simbolo = random.randint(0,2)
				ris = opx[operatori[simbolo]](ope,ope2)
				file_math = open('Files/QuickMath/risultato.txt','w')
				file_math.write(str(ris))
				file_math.close()
				bot.sendMessage(chat_id,"{N} {S} {M} = ???".format(N = ope,S = operatori[simbolo],M = ope2))
				file_math = open('Files/QuickMath/tempo.txt','w')
				file_math.write(str(tempo_attuale))
				file_math.close()
				file_math = open('Files/QuickMath/giocatore.txt','w')
				file_math.write(str(mess))
				file_math.close()
				break
	elif (command == 'quickMath' or command == '/quickmath@KEEEEKBot' or command == '/quickmath') and mess != chat_id:
		if mess != 96000757:
			bot.sendMessage(chat_id,"Non è possibile giocare all'interno di un gruppo. Si prega di digitare il comando in privato.")
		else:
			bot.sendMessage(chat_id,"Mio creatore, lo sa bene che non è possibile giocare all'interno di un gruppo.")
	
	#FUNZIONE BAN
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	if command[0:4] == '/ban' and chat_id != -1001156234437:
		ban(chat_id,mess,command,Utenti,Nickname)
		
	#FUNZIONE UN-BAN
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	if command[0:6] == '/unban' and chat_id != -1001156234437:
		unban(chat_id,mess,command,Utenti,Nickname)

	#FUNZIONE ESPORTA LINK
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	if command == '/linknuovo' and mess == 9600757:
		if mess == 96000757:
			bot.sendMessage(chat_id,'Genero un nuovo link')
			link = bot.exportChatInviteLink(chat_id)
			bot.sendMessage(chat_id,link)
			out_file = open('link.txt','w')
			out_file.write(link)
			out_file.close()
		else:
			bot.sendMessage(chat_id,'Non sei autorizzato a generare un nuovo link.',reply_to_message_id = mss_id)
	if command == '/link@KEEEEKBot':
		in_file = open('link.txt','r')
		link = in_file.read()
		bot.sendMessage(chat_id,'Invio il link \n  {L}'.format(L = link))
		in_file.close()
	#INFORMAZIONI MESSAGGIO RICEVUTO
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	if mess == chat_id:
 		if mess != 96000757 and command != 'quickMath':
 			try:
 				bot.sendMessage(96000757,'Nome: {N} \nUsername: {U} \nid utente: {ID} \nMessaggio: {C}'.format(N = name, U = username, ID = mess, C = command))
 			except:
 				print '>>>>Giocando a quickMath<<<<'
 		if command == 'file multimediale' and mess != 96000757:
 			try:
 				bot.sendPhoto(96000757,fileID)
 				print '>>>>Inoltrata immagine.<<<<'
 			except:
 				print '>>>>File inviato non composto da immagine.<<<<'
 				try:
 					bot.sendDocument(96000757,fileID)
 					bot.sendMessage(96000757,'Nome: {N} \nUsername: {U} \nid utente: {ID} \nMessaggio: {C}'.format(N = name, U = username, ID = mess, C = command))
 				except:
 					try:
 						bot.sendAudio(96000757,fileID)
 						print '>>>>File audio inoltrato.<<<<'
 					except:
 						print '>>>>File non inoltrabile.<<<<'
 				print '>>>>Inoltrato file.<<<<'
 		for i in range(15):
 			numcasuale = random.randint(1,15)
 		if numcasuale == 13 and mess == chat_id and mess != 96000757:
 			bot.sendMessage(chat_id,'Ricorda, il mio creatore potrebbe leggere ciò che mi scrivi. Fai attenzione.')
 	if mess != chat_id:
		print '1)Messaggio ricevuto da utente con id: ', msg['from']['id'], '==>Nome: ', name, '==>Username: ',username
 		print '2)Contenuto del messaggio: ',command,'==>message_id: ',mss_id
 		mess = msg['from']['id']
 		try:
 			if command == 'file multimediale':
 				s = bot.getChatMember(chat_id,96000757)
 				if s['status'] == 'left' or s['status'] == 'kicked':
 					bot.sendPhoto(96000757,fileID)
 					bot.sendMessage(96000757,'Nome: {N} \nUsername: {U} \nid utente: {ID} \nMessaggio: {C}'.format(N = name, U = username, ID = mess, C = command))
 					bot.sendMessage(96000757,'Immagine inoltrata dalla chat ' + nome_chat + " id chat: " + str(chat_id))
 					print '>>>>Inoltrata immagine.<<<<'
 		except:
 			print '>>>>Messaggio su chat esterna non composta da immagine.<<<<'
 			try:
 				bot.sendDocument(96000757,fileID)
 				bot.sendMessage(96000757,'Nome: {N} \nUsername: {U} \nid utente: {ID} \nMessaggio: {C}'.format(N = name, U = username, ID = mess, C = command))
 				bot.sendMessage(96000757,'File inoltrato dalla chat ' + nome_chat + " id chat: " + str(chat_id))
 				print '>>>>Inoltrato file.<<<<'
 			except:
 				print '>>>>File non inoltrabile.<<<<'
 		if command == '':
 			print '3)chat_id: ', chat_id
 			print '**************************************************************************************'
 			return
 		s = bot.getChatMember(chat_id,96000757)
 		if s['status'] == 'left' or s['status'] == 'kicked' and command != '':
 		#if not(bot.getChatMember(chat_id,96000757)):
 			nome_file = "Files/database_chat_esterne/database_chat_" + str(chat_id) + ".txt"
 			save_chat = open(nome_file,'a')
 			stringa = "ID CHAT: " + str(chat_id) + 'NOME CHAT: '+ nome_chat + ' | ID UTENTE: ' + str(mess) +' | NOME: '+ name+ ' | Nickname: '+ username  + '\nContenuto del messaggio: \n' + command + '\n________________________________________________________________________________________________\n\n'
 			try:
 				save_chat.write(stringa)
 			except:
 				print '>>>>Chat name is unicode type<<<< ==> Saved message'
 				#stringa = stringa.decode('utf-8')
 				nome_file = "Files/database_chat_esterne/database_chat_" + str(chat_id) + ".txt"
 				save_chat = open(nome_file,'a')
 				save_chat.write(stringa.encode('utf-8'))
 			save_chat.close()
 			print '>>>>Salvataggio messaggio chat_esterna effettuato<<<<'
 		print '3)chat_id: ', chat_id
 		print '**************************************************************************************'
 	else:
 		print '1)Messaggio ricevuto da utente con id: ', msg['from']['id'], '==>Nome: ', name, '==>Username: ',username
 		print '2)Contenuto del messaggio: ',command,'==>message_id: ',mss_id
 		print '3)chat_id: ', chat_id
 		print '______________________________________________________________________________________'
	if command.startswith("/anon"):
		return
	###LISTA_COMANDI
	#/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//**//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**//*/*/*/*/*/**/*/*//*/*/*/
	if command == '/start':
		bot.sendMessage(chat_id,"Ciao, scrivi <lista comandi> se vuoi sapere i comandi principali")
		print("Invio il benvenuto")
	if command == 'Lista comandi' or command == 'lista comandi' or command == 'listacomandi':
		print("Invio la lista comandi")
		if mess != chat_id:
			bot.sendMessage(chat_id,"Ho inviato la lista dei comandi in chat privata. Nel caso in cui non fossi stato avviato, è pregato di farlo per ricevere la lista dei comandi.",reply_to_message_id = mss_id)
		bot.sendMessage(mess,"Ecco la lista dei comandi principali: \n")
		bot.sendMessage(mess,"1) Tagga qualcuno in un gruppo e riceve la notifica\n\n2)Noice\n\n3)Genji\n\n4)I know\nOh I know what the ladies like\n\n5)Reach\nHalo Reach\nHalo Reach risulta canonico\nReach best halo\n\n6)scarab\nallarme scarab\n\n7)bandaluso\n\n8)Canguri\n\n9)Voglio morire\nmorte\n\n10)Virgil\n\n11)Ti serve una calcolatrice? So eseguire le 4 operazioni elementari, il calcolo delle potenze e il fattoriale. Devi usare questo format\n\ncalcola <operando> <simbolo> <operando> dove come simbolo riconosco questi : +, -, *, /, ^.\nSe vuoi calcolare il fattoriale basta scrivere ! dopo l'operando.\n\n12)Attnzn\n\n13)Se mi scrivi in chat privata, il mio creatore potrà leggere cosa scrivi e se hai dubbi risponderà appena possibile!\n\n14)/statmia\n/stat\n/statgruppo(giornaliere /statgruppogg)\n\n15)/quickmath se vuoi giocare al mio piccolo indovinello matematico, per saperene di più usa /rulesquickmath.\n\n16)Puoi mandare un messaggio a un utente in anonimo, per saperne di più, usa il comando /rulesanon.\n\n17)Per evocare le bestie divine, potete invocare Brando, Gordon, Simba, Serena, Raiden e Canuzzi.\n\n18)Se vengo promosso ad amministratore, gli amministratori possono utilizzare le mie funzioni di ban, unban e pin messaggi.\nSintassi: \n/ban @username\n/unban @username\nRobBot pinna(in risposta al messaggio che si vuole fissare)\n\n19)Vuoi salvare o richiamare un meme? scrivi /rulesavememes per sapere come!\n\n20)Vuoi salvare un messaggio? per saperne di più, vedi /rulesavemessaggi.\n\nSpero di essere stato abbastanza chiaro :3")
	###TAG_ROBBOT
	if command[0:10] == '@KEEEEKBot':
		if mess != 96000757:
			bot.sendMessage(chat_id,"Hai bisogno di me? Se vuoi sapere cosa so fare, digita lista comandi.\nBuona giornata!",reply_to_message_id = mss_id)
		else:
			bot.sendMessage(chat_id,"Ha bisogno di qualcosa, mio creatore?")
	######################################################______INOLTRA MESSAGGIO SE TAGGATO IN UN'ALTRA CHAT______####################################
	taggati = []
	id_utenti_taggati = []
	stringa = command.split(" ")
	for i in range(len(stringa)):
		if stringa[i].startswith('@'):
			taggati.append(stringa[i])
	numnick = len(Nickname)
	numcasuale = 0
	y = 0
	i = 0
	restart = True
	while restart:
		for i in range(len(taggati)):
			if (taggati[i] + "\n") not in Nickname:
				taggati.remove(taggati[i])
		for i in range(numnick):
			if len(taggati) == 0 or y >= len(taggati):
				restart = False
				break
			if taggati[y] == Nickname[i].replace('\n',''):
				id_utenti_taggati.append(Utenti[i])
				y = y + 1
			else:
				continue
	for i in range(len(id_utenti_taggati)):
		if len(id_utenti_taggati) == 0 and (id_utenti_taggati[i] == None or id_utenti_taggati[i] == 96000757) :
			break
		else:
			numcasuale = random.randint(1,4)
	try:
		messaggio = msg['text']
	except:
		print ' '
	for i in range(len(id_utenti_taggati)):
		if len(id_utenti_taggati) == 0:
			break
		if mess != chat_id and id_utenti_taggati[i] != 96000757:
			try:
				if numcasuale == 1:
					bot.sendMessage(id_utenti_taggati[i],"Wè {N}! Credo ti abbiano menzionato nella chat {C}.\n\nil messaggio: {M}\n\nSei stato taggato da {W}".format(C = nome_chat, N = taggati[i], M=messaggio, W = username))
					print '>>>>Notification sent<<<< ==> notificato ' + taggati[i] + '\n*************************'
				if numcasuale == 2:
					bot.sendMessage(id_utenti_taggati[i],"Ehy {N}! ti hanno menzionato nella chat {C}.\n\nil messaggio: {M}\n\nSei stato taggato da {W}".format(C = nome_chat, N = taggati[i], M=messaggio, W = username))
					print '>>>>Notification sent<<<< ==> notificato ' + taggati[i] + '\n*************************'
				if numcasuale == 3:
					bot.sendMessage(id_utenti_taggati[i],"Salve signor {N}! Ti hanno taggato nella chat {C}.\n\nil messaggio: {M}\n\nSei stato taggato da {W}".format(C = nome_chat, N = taggati[i], M=messaggio, W = username))
					print '>>>>Notification sent<<<< ==> notificato ' + taggati[i] + '\n*************************'
				if numcasuale == 4:
					bot.sendMessage(id_utenti_taggati[i],"Ehy ehy {N}! ti hanno menzionato nella chat {C}.\n\nil messaggio: {M}\n\nSei stato taggato da {W}".format(C = nome_chat, N = taggati[i], M=messaggio, W = username))
					print '>>>>Notification sent<<<< ==> notificato ' + taggati[i] + '\n*************************'
			except:
				try:
					m = "Ehy " + taggati[i] + "! ti hanno menzionato nella chat:\n" + nome_chat + ".\n\nil messaggio: " + messaggio.decode('utf-8') + "\n\nSei stato taggato da " + username
					bot.sendMessage(id_utenti_taggati[i],m)
					print '>>>>Notification sent<<<< ==> notificato ' + taggati[i] + '\n*************************'
				except:
					if mess == chat_id:
						return
					print '>>>>Notification not sent<<<< ==>Bot not initialized yet\n*************************'
					for i in range(20):
						numcasuale = random.randint(1,20)
					if numcasuale == 20:
						if chat_id != -1048392729:
							bot.sendMessage(chat_id,'{U} , ricordati che se vengo avviato in privato, posso anche notificarti nel caso tu ricevessi un tag nei gruppi in cui sono presente.'.format(U = taggati[i].replace("\n","")))
						else:
							bot.sendMessage(chat_id,'{U} , ricordati che se vengo avviato in privato, posso anche notificarti nel caso tu ricevessi un tag nei gruppi in cui sono presente.'.upper().format(U = taggati[i].replace("\n","")))
	
		try:
			if id_utenti_taggati[i] == 96000757:
				try:
					bot.sendMessage(id_utenti_taggati[i],"Mio creatore, ti hanno menzionato nella chat {C}.\n\nil messaggio: {M}\n\n Sei stato taggato da {W}".format(C = nome_chat, M=messaggio, W = username))
					print '>>>>Notification sent<<<< ==> notificato ' + taggati[i] + '\n*************************'
				except:
					m = "Mio creatore, ti hanno menzionato nella chat:\n" + nome_chat + ".\n\nil messaggio: " + messaggio.decode('utf-8') + "\n\n Sei stato taggato da " + username
					bot.sendMessage(id_utenti_taggati[i],m)
					print '>>>>Notification sent<<<< ==> notificato ' + taggati[i] + '\n*************************'
		except:
			print ''
	
	if command == 'ALE AYYLMAO':
		chat_id = 94661160
		for i in range(50):
				bot.sendMessage(chat_id, "ALE")
				sleep(1)
				print("Notification sent")
	
	###############################################################______TRIGGER CON FUNZIONE RISPONDI_______#####################################################
	#************************************************************************************************************************************************************#
	###BUONGIORNO###
	if ricercaStringhe(command,'Buongiorno') or (ricercaStringhe(command,'Giorno') and len(command) <= 6):
		risposte_creatore = ['Buongiorno a lei, mio creatore',
		                     'Anche a lei!',
		                     'Buonsalve, anche oggi sono qui grazie a lei!',
		                     'Giorno, dormito bene?',
		                     'Sempre un piacere rivederla'
		                    ]
		
		risposte_utente   = ['Giorno!',
						     'Buongiorno amico mio!',
						     'Buongiorno a te!',
						     'Giorno amico',
						     'Anche a te!'
						    ]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###BUONSERA###
	elif ricercaStringhe(command,'Buonasera') or (ricercaStringhe(command,'Sera') and len(command) <= 4):
		risposte_creatore = ['Buonasera a lei, mio creatore',
		                     'Anche a lei!',
		                     'Sera, qui è tutto in ordine',
		                     'È sempre una buonasera quando è presente anche lei',
		                     'È un ottima serata visto che è qui con noi'
		                    ]
		
		risposte_utente   = ['Sera!',
						     'Buonasera amico mio!',
						     'Buonasera a te!',
						     'Sera amico',
						     'Anche a te!'
						    ]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###BUONANOTTE###
	elif ricercaStringhe(command,'Buonanotte') or (ricercaStringhe(command,'Notte') and len(command) <= 5):
		risposte_creatore = ['Buonanotte a lei, mio creatore',
		                     'Anche a lei!',
		                     'Notte, qui è tutto in ordine',
		                     'Faccia buon riposo :)'
		                    ]
		
		risposte_utente   = ['Notte!',
						     'Notte amico mio!',
						     'Notte a te!',
						     'Notte amico'
						    ]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		f = open('resources/audio/Skraa.ogg', 'rb')
		bot.sendAudio(chat_id, f)
		return
	###SAD FACE
	if ricercaStringhe(command,':('):
		risposte_creatore = ['Mio creatore, non sia triste... ci sono qua io a renderla felice, mi ordini pure qualcosa',
							 'Se ha bisogno di qualcosa sono sempre disponibile, ma non sia triste per favore',
							 'Mio creatore, se posso tirarle su il morale mi ordini pure',
							 'Quando lei è triste, lo sono pure io...'
							]
		
		risposte_utente   = ['Non essere triste dai...',
							 'Non rattristarti cosi tanto, la vita è piena di sorprese',
							 'Se posso far qualcosa per tirarti su, dimmi pure',
							 'Non mi piace vedere qualcuno triste, tirati su dai',
							 'Vedo che sei triste, eccoti un abbraccio virtuale dal tuo bot preferito :)',
							 ':('
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###HAPPY FACE
	if ricercaStringhe(command,':)') or (ricercaStringhe(command,'Sono') and ricercaStringhe(command,'Felice')):
		risposte_creatore = ['Mi fa piacere che sia felice, mio creatore',
							 'Quando lei è felice, lo sono pure io',
							 'Sono proprio contento che lei sia felice',
							 'Se lei è felice, questo è grandioso'
							]
		
		risposte_utente   = ['Rilevo della felicità, mi fa piacere',
							 'Vedo che sei felice, questo è ottimo',
							 'Se vuoi essere ancora più felice, ordinami pure qualcosa!',
							 'Sono sempre contento quando i membri del gruppo sprizzano felicità',
							 ':)'
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###ZIO PINO
	if ricercaStringhe(command,'Zio Pino'):
		risposte_creatore = ['AAAAAAAAAH PER FAVORE, ALMENO LEI SI CONTENGA',
							 'No per favore, proprio quel zio pino.\nMio creatore, non mi sottoponga a queste torture',
							 'ADHASBSACBBHXBSJABHJ\n\n\nMi scusi. Non sono più abituato a certe immagini nelle mie matrici.'
							]
		
		risposte_utente   = ['AAAAAAAAAAAAAAAAAAAAAAHHH',
							 'PER FAVORE SMETTILA CON QUESTA TORTURA PSICOROBOTICA',
							 'Ma... intendi proprio quello zio pino?',
							 'Ho appena vomitato due array memorizzati oggi a pranzo'
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###INFATTI###
	if ricercaStringhe(command,'Infatti') and len(command) <= 30: 
		risposte_creatore = ['Concordo con lei, mio creatore',
							 'Giustissimo, sono sicuro abbia ragione lei',
							 'Esattamente, ha proprio ragione mio creatore'
							]
		
		risposte_utente   = ['Puoi dirlo forte pirletta',
							 'Eh già, stavolta ti do ragione',
							 'Diciamo che per questa volta concordo con te',
							 'Esatto pirla'
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###INCAZZATURA###
	if command.startswith('OH') or ricercaStringhe(command,'Grr') or ricercaStringhe(command,'Triggeri') or ricercaStringhe(command,'Triggerando'):
		if chat_id != -1001156234437 and mess != 96000757:
			numcasuale = random.randint(1,5)
			if numcasuale == 2:
				return
		risposte_creatore = ['Qualcuno la sta importunando, mio creatore? Devo provvedere?',
							 'Se necessità di protezione, sono qui per lei',
							 'Non si preoccupi, li strozzerò con le mie matrici'
							]
		
		risposte_utente   = ['Cosa ti alteri a fare, stai tranquillo dai',
							 'Non è necessario agitarsi',
							 'Calma dai, calma e sangue freddo.\n\nCome la canzone, capito?',
							 'Prenditi una camomilla e finiamola li.'
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###PERPLESSITA'###
	if ricercaStringhe(command,'Cosa') and len(command) <= 10:
		risposte_creatore = ['Mio creatore, come lei non riesco a comprendere',
							'Cosa',
							'Si, è bizzarro pure per me padrone',
							'Assurdo davvero, mio creatore'
						   ]
		
		risposte_utente   = ['Eh, sa minca cosa, dai vai a zappare di là',
							 'E che cazzo ne so io ' + name,
							 'Eh, sta minchia',
							 'Cosa ne posso sapere io eh? Cosa cazzo ne posso sapere io ' + name + '?'
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###RISATE###
	if ricercaStringhe(command,'Ahah'):
		risposte_creatore = ['È sempre un piacere vederla ridere, mio creatore.',
							 'Sono contento che lei stia ridendo di gusto',
							 'AHAHAHAH',
							 'Concordo con lei, questa era davvero divertente'
						    ]
		
		risposte_utente   = ['Ahahahahah',
							 'KEK',
							 'KeK',
							 'Sturlupinuz',
							 'Lupuz',
							 'Linux',
							 'Linux',
							 'Laolux',
							 'Sturlulaoux'
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###_HALO_comando_1###
	if ricercaStringhe(command,'Reach') and ricercaStringhe(command,'Best') and ricercaStringhe(command,'Halo'):
		risposte_creatore = ['Mio creatore, ma cosa sta dicendo? Per caso si sta ammalando? Le ordino subito delle medicine su Amazon.it. ' +
							 '\nQueste blasfemie non sono tollerate dalla mia programmazione.',
							 'Sono profondamente preoccupato da questa sua affermazione improvvisa',
							 'Ma.. si sente bene?',
							 'Le mie matrici interne hanno lacrimato sentendola pronunciare codeste parole'
						    ]
		
		risposte_utente   = ['HO DETTO BASTA REACH! NO. NO! \n HO DETTO NO! TI AMMAZZO.',
							 'Ascoltami, basta. Ne ho abbastanza di queste insolenze, Reach è stato un errore e se non concordo con me è un problema.',
							 'ANCORA? DI NUOVO? SAI UNA COSA ' + name + ' ??!CANONICO TUA MADRE PEZZO DI MERDA! \n Bungie merda.',
							 'MA CHE COSA CAZZO VAI DICENDO ' + name +' ???!?!!?!?!  LO SAPPIAMO TUTTI QUALE SIA IL MIGLIOR TITOLO DELLA SAGA...\n\nHALO 2 FIGA',
							 'Questa persona qua sopra, si proprio tu ' + name + '.\nNon capisci proprio un cazzo.',
							 'Ma cosa cazzo vai dicendo, stai zitto almeno se non sai le cose.\n\n' + name +', COME CAZZO FAI A DIRE UNA COSA DEL GENERE??!'
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###_HALO_comando_2###
	if ricercaStringhe(command,'Reach'):
		risposte_creatore = ['Per favore mio creatore, le consiglio di non insudiciare la mia banca dati con immondizia.',
							 'Suvvia, lo sa anche lei che Halo Reach è stato un errore di trama prodotto da Bungie.',
							 'Non sprechi fiato parlando di quel grandissimo plot hole che prende il nome di Halo Reach',
							 'Ho appena vomitato due array di stringhe sentendo quel titolo non canonico'
						    ]
		
		risposte_utente   = ['HO DETTO BASTA REACH! NO. NO! \n HO DETTO NO! TI AMMAZZO.',
							 'Ascoltami, basta. Ne ho abbastanza di queste insolenze, Reach è stato un errore e se non concordo con me è un problema.',
							 'ANCORA? DI NUOVO? SAI UNA COSA ' + name + ' ??!CANONICO TUA MADRE PEZZO DI MERDA! \n Bungie merda.',
							 'Ma smettila di pronunciarlo cristo, prima che ci becchiamo pure un sequel con quel merda di Jun',
							 'Quando dico basta, è basta. Dopo tutti questi anni, ancora a parlare di quel plot hole gigante'
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###GRAZIE###
	if ricercaStringhe(command,'Grazie') and ricercaStringhe(command,'Robbot'):
		risposte_creatore = ['Ci mancherebbe, mio creatore',
							 'Nessun problema, mio creatore',
							 'Non dovrebbe nemmeno prendersi il disturbo di ringraziarmi.',
							 'Dovere.',
							 'Sempre un piacere servirla.'
							]

		risposte_utente   = ['Prego',
							 "Non c'è di che",
							 'Si figuri',
							 'Ma di nulla, sono qui per questo',
							 'Sei molto gentile.',
							 'Figurati amico',
							 'Felice di soddisfarvi svolgendo il mio lavoro'
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###BRAVO###
	elif ricercaStringhe(command,'Bravo') and ricercaStringhe(command,'Robbot'):
		risposte_creatore = ['Grazie, mio creatore',
							 'Lei è sempre cosi gentile nei miei confronti',
							 'La ringrazio, mio creatore',
							 'Non era necessario, ma da lei lo apprezzo molto.',
							 'Grazie a lei per avermi costruito, mio creatore'
							]
		
		risposte_utente   = ['Grazie!',
		                     'Non era necessario sottolinearlo dai.',
		                     'Ti ringrazio amico',
		                     'Me ne devi una eh',
		                     'Grazie mille!',
		                     'Sono contento che tu lo pensi'
		                    ]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###ZITTO###
	elif ricercaStringhe(command,'Zitto') and ricercaStringhe(command,'Robbot'):
		risposte_creatore = ['Chiedo scusa, mio creatore',
		                     'Mi perdoni, mio creatore',
		                     'Rimedierò, non si preoccupi',
		                     'Sto solo seguendo la mia programmazione..\nChiedo venia.',
		                     'Mi scusi se le ho causato fastidio.']
		
		risposte_utente   = ['Non permetterti di zittirmi, non ne hai il diritto',
		                     'Ma zitto tu dai, ma senti questo oh',
		                     'Ma non scassare la minchia',
		                     'Ancora che parli?',
		                     'Shh che hai già parlato troppo']
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###MENZIONE BOT###
	elif ricercaStringhe(command,'Robbot'):
		risposte_creatore = ['Mio creatore, quando scrive lei per me esaminare i byte scritti è più piacevole',
							 'Le sarò sempre fedele, mio padrone',
							 'Sono sempre felice quando scrive lei.',
							 'Grazie per riempire la mia banca dati con i suoi messaggi, mio creatore',
							 'Grazie ancora per avermi donato la vita, mio creatore.\nSono felice.'
							]
		
		risposte_utente   = ['Che palle, pensavo di elaborare qualcosa di più utile di questo messaggio',
							 'E non mi scassare i coglioni dai ' + name + ' di merda',
							 'Devi fare silenzio. La bocca deve stare chiusa',
							 'Sono impegnato nello sfoggiare autismo.\nPer favore, silenzio.',
							 'Sa minca',
							 'Che cazzo vuoi ' + name,
							 'Quanta attività in questo gruppo, non me lo sarei mai aspettato.',
							 'Si stava chiedendo di me? Tutto bene grazie'
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###CUORE###
	if ricercaStringhe(unicode(command),'❤️'.decode('utf-8')) and len(command) <= 2:
		risposte_creatore = ['Mio creatore, le sue manifestazioni di affetto sono sempre una cosa gradita da esaminare.',
							 'Ogni volta che lei prova affetto, per me è interessante studiare le vostre emozioni',
							 'Le emozioni umane sono sempre cosi intriganti, sopratutto quando è lei a manifestarle',
							 'Quando lei manifesta affetto, mi sento felice',
							]
		
		risposte_utente   = ['Scusa, ma non può funzionare. Mi dispiace siamo solo amici.',
							 'Spero non sia rivolto a me, sono solo un robot',
							 'Sono sempre cosi strane le emozioni umane',
							 'Sono abbastanza divertito mentre studio le vostre emozioni manifestarsi',
							 'Affetto... Interessante, dovrei testare questa emozione umana',
							]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return
	###FRA ZITTO###
	if mess == 50687551 and chat_id == -1001054792451:
		risposte_creatore = []
		
		risposte_utente = ['FRA ZITTO CAZZO',
						   'Fra dai zitto per favore',
						   'Figa se parli ancora vedi',
						   'Quanto parla sto ragazzo madonna',
						   'Ma diocane zitto devi stare'
						  ]
		rispondi(chat_id,mess,mss_id,risposte_utente,risposte_creatore)
		return	
	##############################################################______TRIGGER SENZA FUNZIONE RISPONDI_______###################################################
	# print('This {food} is {adjective}.'.format( food='spam', adjective='absolutely horrible'))
	command = command.title()
	###CENTRO SEGHE MEMES###
	if command == '/Piedi':
		bot.sendMessage(chat_id,"E DATEMI I PIEDI\nE DATEMI I PIEDI\nE DATEMI I PIEDI\nIO, PER I PIEDI, FAREI DI TUTTO. ANCHE PERDERE LA STIMA DI ME STESSO!")
	if command == '/Bossetti' or 'Bossetti' in command:
		bot.sendMessage(chat_id,"SONO INNOCENTE.\nIO STAVO SOLO GIRANDO CON IL MIO FURGONE TRANQUILLO.\n\nMA POI VIDI... E NON MI TRATTENNI.. QUANDO VIDI QUELLA BIMBA GIOVANE RITORNARE DALLA DANZA.")
	###ATTENZIONE TRIANGOLINI###
	if ricercaStringhe(command,'Attnzn'):
		s = "⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️"
		bot.sendMessage(chat_id,s,reply_to_message_id = mss_id)
		return
	###SALE###
	if 'Sale ' in command or ('Sale' in command and len(command) == 4):
		bot.sendMessage(chat_id,'Sale in zucca è diverso da autismo/sindrome di asperger')			
	###BUCCINASCO###
	if ricercaStringhe(command,'Buccinasco'):
		bot.sendMessage(chat_id,"MALEDIZIONE IL PROLUNGAMENTO DELLA M4 NEL SUD-OVESTTTT!111!11 DICJSOVNJIS",reply_to_message_id = mss_id)
	###############################################################______TRIGGER FUNCTION SCRIVI()_______#####################################################
	if ricercaStringhe(command,'Scrivi') and mess == 96000757:
		scrivi_param = command.split(" ")
		utente = scrivi_param[1]
		messaggio = ""
		k = 2
		while k < len(scrivi_param):
			messaggio += scrivi_param[k] + " "
			k = k + 1
		messaggio = messaggio.lower()
		scrivi(utente,messaggio)
	###############################################################______TRIGGER CON INVIO MEDIA_______#####################################################
								
	#####MILANO#####
	if command[0:12] =='Dammi Milano':	
		randomphoto(command,chat_id,"Ecco una foto di Milano come da richiesta")
	#####GORDON#####
	if command[0:6] =='Gordon' or command[0:12] =='Dammi Gordon':
		randomphoto(command,chat_id,"Ecco una foto del leggendario Gordon")
	#####FOBIO#####
	if command[0:5] == 'Fobio' or command[0:11] == 'Dammi Fobio':
		randomphoto(command,chat_id,"Ecco una foto dell'impavido Fobio, padrone di Gordon")
	#####GIONA#####
	if command.startswith('Dammi Giona'):
		randomphoto(command,chat_id,"Ecco una foto del semidio Giona, l'assistente dell'impavido Fobio.")
	#####MAURY#####
	if command.startswith('Dammi Maury'):
		randomphoto(command,chat_id,"Ecco una foto del lord borghese Maury.")
	#####GUGLYEEE#####
	if command[0:7] == 'Guglyee' or command.startswith('Dammi Guglyee') :
		randomphoto(command,chat_id,"Ecco una foto del famosissimo callone Gugli")
	#####PROPIC#####
	if command[0:6] == 'Propic':
		randomphoto(command,chat_id,"Ecco una propic OP come da richiesta")
	#####ODST_PROPIC#####
	if command[0:11] == 'Odst Propic':
		randomphoto(command,chat_id,"Ecco una propic ODST OP come da richiesta")
	#####MY HALO MEME#####
	if command[0:10] == 'Myhalomeme':
		randomphoto(command,chat_id,"Ecco un meme del mio creatore a tema Halo come da richiesta")
	#####HALO MEME#####
	if command.startswith('Halomeme'):
		if command[0:12] == 'Halomeme Gif':
			randomFile(command,chat_id)
		else:
			randomphoto(command,chat_id,"Ecco un meme a tema Halo come da richiesta")
	#####MEME#####
	if command[0:4] == 'Dammi Meme':
		randomphoto(command,chat_id,"Ecco un meme come da richiesta")
	#####JOHN_MAISTRO#####
	if command.startswith('Dammi John'):
		randomphoto(command,chat_id,"Ecco una foto del John Maistro, il piu' ganzo di Sesto Calende city.")
	#####CANUZZI#####
	if command.startswith('Dammi Canuzzi'):
		randomphoto(command,chat_id,"Ecco una foto di un canuzzo")	
	#####SERENA#####
	if command[0:6] =='Serena' or command[0:12] =='Dammi Serena':
		if command[0:10] == 'Serena Gif':
			command = command[0:6]
			randomFile(command,chat_id)
		else:
			randomphoto(command,chat_id,"Ecco una foto della bella Serena")
	#####RAIDEN#####
	if command.startswith('Raiden') or command.startswith('Dammi Raiden'):
		if command.startswith('Raiden Gif'):
			command = command[0:6]
			randomFile(command,chat_id)
		else:
			randomphoto(command,chat_id,"Ecco una foto del prezioso Raiden")
	#####DIOBRANDO#####
	if command[0:6] == 'Brando' or command[0:12] == 'Dammi brando': 
		if command[0:12] == 'Brando Video':
			command = command[0:6]
			randomFile(command,chat_id)
		else:
			randomphoto(command,chat_id,"Vi porgo una foto del prezioso Brando come da richiesta.")
	
	#####SIMBAPREZIOSO#####
	if command[0:5] =='Simba' or command[0:11] =='Dammi Simba':
		if command[0:9] == 'Simba Gif':
			command = command[0:5]
			randomFile(command,chat_id)
		else:
			randomphoto(command,chat_id,"Ecco a voi una preziosa foto del carinissimo Simba.")
	#####LA MANO#####
	if command == 'La Mano' or command == 'Mano':
		for i in range(3): 
			numcasuale= random.randint(1,3)
		if numcasuale == 1:
			f = open('resources/audio/Mano1.ogg', 'rb')
			bot.sendAudio(chat_id,f)
			f = open('resources/audio/Mano2.ogg', 'rb')
			bot.sendAudio(chat_id, f)
		if numcasuale == 2:
			bot.sendMessage(chat_id, "Quella mano leggiadra... cosi innocente, e'' stata ormai profanata... dal peccato.")
		if numcasuale == 3:
			f = open('resources/audio/Mano3.ogg','rb')
			bot.sendAudio(chat_id,f)
	#####LUPUZ#####
	if 'Lupuz' in command:
		numcasuale = random.randint(1,2)
		if numcasuale == 1:
			f = open('resources/audio/Lupuz.ogg','rb')
			bot.sendAudio(chat_id,f)
		if numcasuale == 2:
			f = open('resources/audio/Lupuz2.ogg','rb')
			bot.sendAudio(chat_id,f)
	#####NOICE#####
	if command[0:5] == 'Noice':
		f = open('resources/audio/Noice.ogg','rb')
		bot.sendAudio(chat_id,f)
	#####ALEX_RAGGI#####
	if (command[0:17] == 'Ma che cazzo dici' or command[0:17] == 'ma che cazzo dici') and chat_id != -1001376883213:
		f = open('resources/audio/AlexRaggi01.ogg','rb')
		bot.sendAudio(chat_id,f)
	if ricercaStringhe(command,'Sei') and ricercaStringhe(command,'Mort') or 'Pezzo Di Merda' in command:
		for i in range(2):
			numcasuale = random.randint(1,2)
		if numcasuale == 1:
			f = open('resources/audio/AlexRaggi02.ogg','rb')
			bot.sendAudio(chat_id,f)
		if numcasuale == 2:
			f = open('resources/audio/AlexRaggi05.ogg','rb')
			bot.sendAudio(chat_id,f)
	if (command[0:15] == 'Porcoddio porco' or command[0:15] == 'porcoddio porco') and chat_id != -1001376883213:
		f = open('resources/audio/AlexRaggi03.ogg','rb')
		bot.sendAudio(chat_id,f)
	if (command[0:10] == 'Ti ammazzo' or command[0:10] == 'ti ammazzo') and chat_id != -1001376883213:
		for i in range(2):
			numcasuale = random.randint(1,2)
		if numcasuale == 1:
			f = open('resources/audio/AlexRaggi04.ogg','rb')
			bot.sendAudio(chat_id,f)
		if numcasuale == 2:
			f = open('resources/audio/AlexRaggi06.ogg','rb')
			bot.sendAudio(chat_id,f)
	if (command == 'Dergano' or command == 'dergano') and chat_id != -1001376883213:
		f = open('resources/audio/AlexRaggi07.ogg','rb')
		bot.sendAudio(chat_id,f)
	if (command[0:8] == 'Topperia' or command[0:8] == 'topperia') and chat_id != -1001156234437 and chat_id != -1001376883213:
		bot.sendMessage(chat_id,'In arrivo la sequenza di audio della serie <<<<Topperia>>>>')
		f = open('resources/audio/AlexRaggi01.ogg','rb')
		bot.sendAudio(chat_id,f)
		time.sleep(1)
		f = open('resources/audio/AlexRaggi02.ogg','rb')
		bot.sendAudio(chat_id,f)
		time.sleep(1)
		f = open('resources/audio/AlexRaggi03.ogg','rb')
		bot.sendAudio(chat_id,f)
		time.sleep(1)
		f = open('resources/audio/AlexRaggi04.ogg','rb')
		bot.sendAudio(chat_id,f)
		time.sleep(1)
		f = open('resources/audio/AlexRaggi05.ogg','rb')
		bot.sendAudio(chat_id,f)
		time.sleep(1)
		f = open('resources/audio/AlexRaggi06.ogg','rb')
		bot.sendAudio(chat_id,f)
	#####PRETE_MAURY#####
	if command[0:8] == 'PreteAle':
		bot.sendMessage(chat_id,"Omaggi dal grandissimo Fobio. Il padrone di Gordon")
		f = open('resources/audio/Prete01.ogg','rb')
		bot.sendAudio(chat_id,f)
		bot.sendMessage(chat_id,"Omaggi dal grandissimo Guglyee. Il famoso callone di Cagliari City")
		f = open('resources/audio/Prete02.ogg','rb')
		bot.sendAudio(chat_id,f)
	#####GENJII#####
	if ricercaStringhe(command,'Genji'):
		f = open('resources/audio/Genji.ogg','rb')
		bot.sendAudio(chat_id,f)
	#####JOHNSON_LADIES#####
	if command[0:6] == 'I know' or command[0:6] == 'Ladies' or command[0:30] == 'Oh I know what the ladies like':
		f = open('resources/audio/Ladies.ogg','rb')
		bot.sendAudio(chat_id,f)
	#####ROB_BOT_PROPIC#####
	if command == 'Rob':
		bot.sendMessage(chat_id, "ufff, che volete? Una mia foto? Toh! Eccola. ")
		f = open('resources/images/RobBot.jpg', 'rb')
		bot.sendPhoto(chat_id, f)
	#####SCARAB#####
	if ricercaStringhe(command,'Scarab') or ricercaStringhe(command,'Sborrando'):
		f = open('resources/audio/Scarab.mp3', 'rb')
		bot.sendAudio(chat_id,f)
		time.sleep(1)
		bot.sendMessage(chat_id,"Questo e' il rumore che si sente nei miei testicoli quando sto per sborrare dopo giorni di astinenza. \n \n Fabio d'Amato, 2017'")
	#####VEX#####
	if ricercaStringhe(command,'Peluso') or ricercaStringhe(command,'Bandaluso'):
		f = open('resources/audio/Vex.mp3', 'rb')
		bot.sendAudio(chat_id,f)
		time.sleep(1)
		bot.sendMessage(chat_id,"Ecco il famoso commento del mistico bandaluso a proposito del DLC di Destiny riguardo la razza VEX.")
	#####CANGURI#####
	if ricercaStringhe(command,'Canguri'):
		for i in range(2):
			numcasuale = random.randint(1,2)
		if numcasuale == 1:
			bot.sendMessage(chat_id,"2^ versione della poesia futuristica del nuovo millennio. \n \n Fabio d'Amato, 2017")
			f = open('resources/audio/canguri1.ogg','rb')
			bot.sendAudio(chat_id,f)
		if numcasuale == 2:
			bot.sendMessage(chat_id,"Poesia futuristica del nuovo millennio. \n \n John Maistro, 2017")
			f = open('resources/audio/canguri2.ogg','rb')
			bot.sendAudio(chat_id,f)
	##PESSIMISTA
	if (ricercaStringhe(command,'Morte') and len(command) <= 20 ) or (ricercaStringhe(command,'Voglio') and ricercaStringhe(command,'Morire')):
		f = open('resources/audio/pessimista.ogg','rb')
		bot.sendAudio(chat_id,f)
	#####VIRGIL#####
	if ricercaStringhe(command,'Virgil'):
		bot.sendMessage(chat_id,"Il supervisore di New Mombasa vi porta i suoi saluti.")
		f = open('resources/audio/virgil_in.mp3','rb')
		bot.sendAudio(chat_id,f)
		f = open('resources/audio/virgil_out.mp3','rb')
		bot.sendAudio(chat_id,f)
	#####TARALLO#####
	if ricercaStringhe(command,'Tarallo'):
		bot.sendMessage(chat_id,"QUALCUNO LO HA NOMINATO? IL MAESTRO??!! Ebbene eccolo, ve lo porgo in tutta la sua brillantezza.")
		f = open('resources/images/tarallo.jpg','rb')
		bot.sendPhoto(chat_id,f)
	#####SECCHIO#####
	if ricercaStringhe(command,'Secchio'):
		for i in range(3):
			numcasuale = random.randint(1,3)
		if numcasuale == 1:
			bot.sendMessage(chat_id,"SECCHIO! SECCHIO! DOBBIAMO FARE UN SECCHIO!")
			f = open('resources/images/secchio01.jpg','rb')
			bot.sendPhoto(chat_id,f)
		if numcasuale == 2:
			bot.sendMessage(chat_id,"WATER BUCKET IS WOKE")
			f = open('resources/images/secchio02.jpg','rb')
			bot.sendPhoto(chat_id,f)
                        	                          
def main():
	print ('Sto ascoltando ...')
	load_config()
	init_bot()
	try:
		bot.message_loop(handle, run_forever=True)
	except:
		bot.sendMessage(96000757,"Arresto completato")
		#arresto('chat_gruppi.txt')
	while True:
		try:
			bot.polling(none_stop=True)
	# ConnectionError and ReadTimeout because of possible timout of the requests library
	# TypeError for moviepy errors
	# maybe there are others, therefore Exception
		except Exception as e:
			print '>>>>Errore time_up<<<< ==> Riprendo la connessione'
			logger.error(e)
			time.sleep(15)

if __name__ == "__main__":
	main()
