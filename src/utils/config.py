import ConfigParser


_conifg = None

def load_config():
	global _config
	_config = ConfigParser.ConfigParser()

	_config.read('config.ini')

def get_config(section, option):
	try:
		return _config.get(section, option)
	except configparser.NoSectionError:
		raise Exception('cannot find section {}'.format(section))
	except configparser.NoOptionError:
		raise Exception('cannot find option {}'.format(option))
