import java.io.*;
import java.util.*;
public class OrdinaFile {
	public static void main(String[]args) throws IOException{
		String id = args[0];
		String nomi = args[1];
		String username = args[2];
		String messaggi = args[3];
		File FileID = new File(id);
		File FileNomi = new File(nomi);
		File FileUsername = new File(username);
		File FileMessaggi = new File(messaggi);
		Scanner scanID = new Scanner(FileID);
		Scanner scanNomi = new Scanner(FileNomi);
		Scanner scanUsername = new Scanner(FileUsername);
		Scanner scanMessaggi = new Scanner(FileMessaggi);
		ArrayList<String> ID = new ArrayList<String>();
		ArrayList<String> Nomi = new ArrayList<String>();
		ArrayList<String> Username = new ArrayList<String>();
		ArrayList<String> Messaggi = new ArrayList<String>();
		int k = 0;
		String save = "                                            ";
		while(scanID.hasNext()){
			ID.add(k,scanID.nextLine());
			Nomi.add(k,scanNomi.nextLine());
			Username.add(k,scanUsername.nextLine());
			Messaggi.add(k,scanMessaggi.nextLine());
			k = k + 1;
		}
		try{
			File scrittura = new File("Data_Utenti_Ordinati.txt");
			FileWriter write = new FileWriter(scrittura);
			PrintWriter out = new PrintWriter(write);
			System.out.println("Salvataggio su file in corso...");
			out.println("ID UTENTE          NOME          USERNAME     \t\t\t\t\t     NUMERO MESSAGGI");
			out.println("__________________________________________________________________________________");
			String nome = "";
			int len = 33;
			String user = "";
			for(int i = 0;i < ID.size();i++){
				user = Username.get(i);
				while(user.length() < len){
					user += " ";
					if(user.length() == len){
						Username.set(i,user);
						break;
					}
				}
			}
			Integer conteggio = new Integer(0);
			int totUtenti = ID.size() - 1;
			int totUtentiComplete = 0;
			for(int i = 0;i < ID.size();i++){
				conteggio += Integer.parseInt(Messaggi.get(i));
				if(Nomi.get(i).equals("nome non ancora ottenuto") || Username.get(i).trim().equals("username non ancora ottenuto")){
					ID.remove(ID.get(i));
					Nomi.remove(Nomi.get(i));
					Username.remove(Username.get(i));
					Messaggi.remove(Messaggi.get(i));
					continue;
				}
				else if(Nomi.get(i).length() > 9){
					if (i == 426)
						nome = "non valido.";
					else{
						nome = Nomi.get(i);
						nome = nome.substring(0,9);
//						out.println(ID.get(i) + "\t"+"\t" + nome + "\t"+"\t" + Username.get(i) + "\t"+"\t" + Messaggi.get(i).trim());
					}
					out.println(ID.get(i) + "\t"+"\t" + nome + "\t"+"\t" + Username.get(i) + "\t"+"\t" + Messaggi.get(i).trim());
				}
				else if(Nomi.get(i).length() == 9){
					out.println(ID.get(i) + "\t"+"\t" + Nomi.get(i) + "\t"+"\t" + Username.get(i) + "\t"+"\t" + Messaggi.get(i).trim());
				}
				else if(Nomi.get(i).length() < 4){
					out.println(ID.get(i) + "\t"+"\t" + Nomi.get(i) + "\t"+"\t"+"\t"+"\t" + Username.get(i) + "\t"+"\t" + Messaggi.get(i).trim());
				}
				else if(Nomi.get(i).length() == 8){
					out.println(ID.get(i) + "\t"+"\t" + Nomi.get(i) + "\t"+"\t" + Username.get(i) + "\t"+"\t" + Messaggi.get(i).trim());
				}
				else
					out.println(ID.get(i) + "\t"+"\t" + Nomi.get(i) + "\t"+"\t"+"\t" + Username.get(i) + "\t"+"\t" + Messaggi.get(i).trim());
				totUtentiComplete++;
			}
			out.println("_____________________________________________________________________________________________________________________");
			//int totUtentiComplete = ID.size() - 6;
			out.println("Totale utenti: " + totUtenti + "\n" + "Totale utenti con dati completi: " + totUtentiComplete + "\n" + "Totale messaggi ricevuti: " + conteggio);
			out.close();
			System.out.println("Salvataggio completato con successo!");
		}
		catch(IOException e){
			System.out.println("Errore imprevisto durante scrittura file.");
		}
		
	}
}
