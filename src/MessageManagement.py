# coding=utf-8
import time
import telepot
import telebot
import commands
import random
import traceback
import time
from datetime import date 
import operator
global bot
bot = telepot.Bot('133326326:AAHCjdG48vALFV-8iqmIammHMbHWcEkx-mM')

"""
	La seguente funzione serve per salvare un determinato messaggio all'interno di una chat di gruppo.
	
	parametri: id della relativa chat, id del messaggio da salvare, un nome simbolico con cui richiamare il messaggio che verrà salvato.
"""
def savemessage(chat,reply_mss_id,richiamo):
	file_msg = open('Files/Messaggi/Messaggi_salvati.txt','a')
	file_msg.write(str(richiamo) + ";" + str(reply_mss_id) + ";" + str(chat))
	file_msg.write('\n')
	file_msg.close()
	bot.sendMessage(chat,"Messaggio salvato",reply_to_message_id = reply_mss_id)
	print '>>>>Messaggio salvato con successo<<<<'
	return

"""
	La seguente funzione serve per recuperare un messaggio salvato in precedenza.
	
	parametri: id della chat, nome simbolico con cui è stato salvato il messaggio.
	
	Una volta recuperati tutti i messaggi dal file di testo, avviene lo split per ";" e dopo aver controllato che la chat corrisponda alla chat del messaggio,
	viene restituito il messaggio desiderato.
"""
def pickmessagesaved(chat,richiamo):
	file_msg = open('Files/Messaggi/Messaggi_salvati.txt','r')
	lista_saved = []
	while True:
		msg = file_msg.readline()
		if not msg:
			break
		lista_saved.append(msg)
	file_msg.close()
	try:
		for i in range(len(lista_saved)):
			if lista_saved[i].startswith(richiamo):
				trovato = lista_saved[i]
				filtro = lista_saved[i].split(";")
				if int(filtro[2]) != chat:
					bot.sendMessage(chat_id,"Messaggio non esistente su questa chat.")
				mss_id = filtro [1]
				bot.sendMessage(chat,"Ecco il messaggio",reply_to_message_id = mss_id)
				print '>>>>Messaggio recuperato con successo<<<<'
				return
	except:
		bot.sendMessage(chat,"Non è stato possibile recuperare il messaggio")
	return

"""
	La seguente funzione permette di cancellare un messaggio salvato in precedenza.
	
	parametri: id della chat, nome simbolico con cui si era salvato il messaggio cosi da ritrovarlo e cancellarlo.
	
	Procedura analoga alla funzione pickmessagesaved, con la differenza che il messaggio una volta trovato viene cancellato dalla lista.
"""
def deleteMessage(chat,richiamo):
	file_msg = open('Files/Messaggi/Messaggi_salvati.txt','r')
	lista_saved = []
	while True:
		msg = file_msg.readline()
		if not msg:
			break
		lista_saved.append(msg)
	file_msg.close()
	try:
		for i in range(len(lista_saved)):
			if lista_saved[i].startswith(richiamo):
				trovato = lista_saved[i]
				filtro = lista_saved[i].split(";")
				if int(filtro[2]) != chat:
					bot.sendMessage(chat_id,"Messaggio non esistente su questa chat.")
				mss_id = filtro [1]
				lista_saved.remove(lista_saved[i])
				bot.sendMessage(chat,"Messaggio cancellato.",reply_to_message_id = mss_id)
				print '>>>>Messaggio cancellato con successo<<<<'
				break
		file_msg = open('Files/Messaggi/Messaggi_salvati.txt','w')
		for i in range(len(lista_saved)):
			file_msg.write(lista_saved[i])
		file_msg.close()		
		return
	except:
		bot.sendMessage(chat,"Si è verificato un errore durante la cancellazione del messagio.")
	return
	
"""
	La seguente funzione restituisce la lista dei messaggi salvati all'interno della chat di gruppo in cui viene chiamata.
	
	parametri: id della chat, id del messaggio in cui viene chiamata la funzione.
	
	Viene letta la lista completa dal file contenente tutti i messaggi. Vengono splittati per ";" e filtrati per id chat e vengono restituiti solo quelli.
"""
def listaMessaggiSalvati(chat,mss_id):
	file_msg = open('Files/Messaggi/Messaggi_salvati.txt','r')
	lista_saved = []
	lista_messaggi = []
	while True:
		msg = file_msg.readline()
		if not msg:
			break
		lista_saved.append(msg)
	for i in range(len(lista_saved)):
		filtro = lista_saved[i].split(";")
		filtro[2] = filtro[2].replace("\n","")
		if filtro[2] == str(chat):
			lista_messaggi.append(filtro[0])
	messaggio = "Ecco la lista dei messaggi salvati su questa chat.\nPer richiamarli è sufficiente digitare <Richiama> <Messaggio>\nEsempio: Richiama "+lista_messaggi[0]+"\n\n"
	for i in range(len(lista_messaggi)):
		messaggio += str(i+1) + ") " + lista_messaggi[i] + "\n"
	bot.sendMessage(chat,messaggio)
	return
