# coding=utf-8
import time
import telepot
import telebot
import commands
import random
import traceback
import time
from datetime import date 
import operator
global bot
bot = telepot.Bot('133326326:AAHCjdG48vALFV-8iqmIammHMbHWcEkx-mM')

"""
	La seguente funzione prende come parametri l'id della chat dove viene chiamata, l'id dell'utente che la chiama, tutto il contenuto del messaggio di chiamata,
	l'array contenente tutti gli id di tutti gli utenti registrati e l'array contenente tutti i nickname di tutti gli utenti registrati.
	
	Viene prima di tutto verificato che l'utente che ha chiamato la funzione abbia il titolo di creator o administrator all'interno di quello specifico gruppo 
	in cui è stata chiamata la funzione. In caso di riscontro negativo, la funzione terminerà con un messaggio di autorizzazione insufficiente.
	
	Una volta effettuata la verifica, la funzione procederà a ricercare il nickname desiderato da bannare tra tutti i nickname registrati. Una volta trovato, 		 
	l'utente in questione verrà kickato dal gruppo con effetto immediato. Nel caso in cui l'utente non verrà trovato, la funzione terminerà con un messaggio.
	
	Durante la ricerca del nickname, viene inoltre controllato alla fine se l'utente desiderato è effettivamente all'interno del gruppo. In quel caso si procederà
	al ban, altrimenti la funzione terminerà con un messaggio.
	
	extra: vi è un controllo in più che non permette al bot di kickare il proprio creatore @MasterCruelty.
	
"""
def ban(chat,id_caller,messaggio,utenti,nickname):
	user_ban = messaggio[5:]
	user = bot.getChatMember(chat,id_caller)
	if user['status'] == 'creator' or user['status'] == 'administrator' or id_caller == 96000757:
		for i in range(len(utenti)):	
			if user_ban == nickname[i].replace('\n',''):
				id_ban = utenti[i]
				save_ban = user_ban
				user_ban = bot.getChatMember(chat,id_ban)
				if id_ban == 96000757:
					bot.sendMessage(chat,"Non posso bannare il mio creatore. Mi dispiace, va contro la mia programmazione.")
					return
				if user_ban['status'] == 'member':
					bot.kickChatMember(chat,id_ban)
					bot.sendMessage(chat,"Utente "+ save_ban + " bannato con successo!")
					return
				else:
					bot.sendMessage(chat,"L'utente " + save_ban + " non è presente su questo gruppo!")
			elif i == len(utenti) - 1:
				bot.sendMessage(chat,"Utente non trovato.")
				return
	else:
		bot.sendMessage(chat,"Non sei autorizzato ad utilizzare questo comando.")
		return

"""
	La seguente funzione prende come parametri l'id della chat dove viene chiamata, l'id dell'utente che la chiama, tutto il contenuto del messaggio di chiamata,
	l'array contenente tutti gli id di tutti gli utenti registrati e l'array contenente tutti i nickname di tutti gli utenti registrati.
	
	Viene prima di tutto verificato che l'utente che ha chiamato la funzione abbia il titolo di creator o administrator all'interno di quello specifico gruppo 
	in cui è stata chiamata la funzione. In caso di riscontro negativo, la funzione terminerà con un messaggio di autorizzazione insufficiente.
	
	Una volta effettuata la verifica, la funzione procederà a ricercare il nickname desiderato da sbannare tra tutti i nickname registrati. Una volta trovato, 		 
	l'utente in questione verrà sbannato(dunque sarà possibile aggiungerlo di nuovo) dal gruppo con effetto immediato. 
	Nel caso in cui l'utente non verrà trovato, la funzione terminerà con un messaggio.
	
"""
def unban(chat,id_caller,messaggio,utenti,nickname):
	user_unban = messaggio[7:]
	user = bot.getChatMember(chat,id_caller)
	if user['status'] == 'creator' or user['status'] == 'administrator' or id_caller == 96000757:
		for i in range(len(utenti)):	
			if user_unban == nickname[i].replace('\n',''):
				id_unban = utenti[i]
				bot.unbanChatMember(chat,id_unban)
				bot.sendMessage(chat,"Utente "+ user_unban + " sbannato con successo!")
				return
			elif i == len(utenti) - 1:
				bot.sendMessage(chat,"Utente non trovato.")
				return
	else:
		bot.sendMessage(chat,"Non sei autorizzato ad utilizzare questo comando.")
		return

"""
	La seguente funzione permette al bot di fissare un determinato messaggio all'interno dei gruppi.
	
	Requisiti: il bot deve essere amministratore e avere i poteri di fissare i messaggi. Il gruppo deve essere un supergruppo.
	
	Parametri: id utente che ha chiesto di fissare, id della relativa chat, id del messaggio da fissare
"""
def pinMessage(mess,chat,reply_mss_id):
	if mess == chat:
		bot.sendMessage(chat,"Non puoi fissare un messaggio in una chat privata con me. Prova all'interno di un gruppo in cui sono amministratore.")
	else:
		try:
			bot.pinChatMessage(chat,reply_mss_id,disable_notification=True)
			bot.sendMessage(chat,"Messaggio pinnato con successo!",reply_to_message_id = reply_mss_id)
			print '>>>>Messaggio fissato con successo<<<<'
		except:
			bot.sendMessage(chat,"Ci deve essere qualche errore. Non riesco a fissare il messaggio desiderato, probabilmente non sono amministratore oppure non ho i poteri adeguati per fissare messaggi su questo gruppo.\nPer poter utilizzare questa funzione è bene controllare che io sia stato promosso ad amministratore con gli adeguati poteri, inoltre il gruppo deve essere del tipo <supergruppo>.")
			print '>>>>Errore su pinMessage<<<<'
	return

"""
	La seguente funzione permette al bot di intervenire eventualmente se all'interno della chat di gruppo si verificano periodi di inattività per alcune ore.
	 
"""
def silenzio():
	chat_da_controllare = open('chat_gruppi.txt','r')
	chat_gruppi = []
	
	###LOOP SULLA LISTA DI CHAT SALVATE
	
	while True:
		chat = chat_da_controllare.readline()
		if not chat:
			break
		chat_gruppi.append(chat.replace("\n",""))
	chat_da_controllare.close()
	data_messaggio = time.strftime("%d/%m/%Y")
	path = 'Files/Logger/Logger_' + data_messaggio.replace('/','_')
	check_logger = open(path,'r')
	orari_messaggi_chat = []
	orario_corrente = str(time.strftime("%H:%M:%S"))
	
	###LOOP SUL LOGGER IN DATA ODIERNA PER RECUPERARE TUTTI I MESSAGGI
	
	while True:
		riga = check_logger.readline()
		if not riga:
			break
		riga = riga.split(";")
		for i in range(len(chat_gruppi)):
			if str(chat_gruppi[i]) == riga[0]:
				orari_messaggi_chat.append(riga[0] + ";" + riga[4])
	check_logger.close()
	orario_messaggio_chat = []
	
	###LOOP PER FAR COMBACIARE I MESSAGGI DEL LOGGER ALLA PROPRIA CHAT
	
	for i in range(len(chat_gruppi)):
		k = 0
		while k < len(orari_messaggi_chat):
			orari_messaggi = orari_messaggi_chat[k].split(";")
			if chat_gruppi[i] == orari_messaggi[0] and k < len(orari_messaggi_chat):
				orario_messaggio_chat.append(orari_messaggi[1])
			elif k == len(orari_messaggi_chat) - 1:
				orario_confronto = orario_messaggio_chat[len(orario_messaggio_chat)-1]
				if (int(orario_corrente[0:2]) - int(orario_confronto[0:2])) > 4 and int(orario_confronto[0:2]) > 9:
					try:
						bot.sendMessage(chat_gruppi[i],"Silenziosi oggi eh")
						logger(chat_gruppi[i],"funzione_silenzio","@funzione_silenzio",123456,"Silenzio confermato su questa chat")
						print '>>>>Log di conferma invio silenzio salvato con successo<<<<'
						k = 0
					except:
						print '>>>>Non vi sono diritti per inviare messaggi su questa chat<<<<'
			k = k + 1
