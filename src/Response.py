# coding=utf-8
import time
import telepot
import telebot
import commands
import random
import traceback
import time
from datetime import date 
import operator
global bot
bot = telepot.Bot('133326326:AAHCjdG48vALFV-8iqmIammHMbHWcEkx-mM')

"""
	La seguente funzione serve a cercare una parola specifica all'interno di un messaggio
	
	parametri: il messaggio completo su cui effettuare la ricerca, la parola da cercare.
"""
def ricercaStringhe(messaggio,stringa):
	messaggio = messaggio.title()
	stringa = stringa.title()
	parole = messaggio.split(" ")
	for i in range(len(parole)):
		try:
			if unicode(stringa) in unicode(parole[i]):
				return True
		except:
			print '>>>>Warning durante matching parole<<<<'
	return False

"""
	La seguente funzione permette al bot di effettuare risposte dinamiche prese in modo casuale da una lista di stringhe.
	
	parametri: id chat, id utente, id messaggio, lista risposte per gli utenti, lista risposte per 96000757
"""
def rispondi(chat,mess,mss_id,risposte_utente,risposte_creatore):
	risposta = random.randint(0,len(risposte_utente)-1)
	risposta_creatore = random.randint(0,len(risposte_creatore)-1)
	probability_risposta = random.randint(1,20)
	if mess != 96000757:
		messaggio = risposte_utente[risposta]
		bot.sendMessage(chat,messaggio,reply_to_message_id = mss_id)
		return
	else:
		messaggio = risposte_creatore[risposta_creatore]
		bot.sendMessage(chat,messaggio,reply_to_message_id=mss_id)
		return
