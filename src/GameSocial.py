# coding=utf-8
import time
import telepot
import telebot
import commands
import random
import traceback
import time
from datetime import date 
import operator
global bot
bot = telepot.Bot('133326326:AAHCjdG48vALFV-8iqmIammHMbHWcEkx-mM')

"""
	La seguente funzione prende come parametro l'operando e permette il calcolo del fattoriale su di esso. Restituisce infine il risultato
	Questa funzione appoggia il calcolo del fattoriale nella funzione 'calcolare'.
"""
def fattoriale(operando):
	risultato = 1
	while operando != 0:
		risultato = risultato * operando
		operando = operando - 1
	return risultato
	
"""
	La seguente funzione permette di effettuare calcoli multipli all'interno della chat. Simboli ammessi: +,-,*,/,**
	
	parametri: id chat, id message, operandi, simboli e la stringa iniziale da stampare a fine procedura
	
	Questa funzione appoggia il calcolo dei risultati nel gioco 'QuickMath'.
"""
def calcolare(chat_id,mss_id,simboli,operandi,command):
	risultato = 0
	stringa = command
	for i in range(len(simboli)):
		try:
			if operandi[i] == '+' and i == 0:
				risultato += int(simboli[i]) + int(simboli[i+1])
			elif operandi[i] == '+' and i != 0:
				risultato = int(risultato) + int(simboli[i+1])
			
			if operandi[i] == '-' and i == 0:
				risultato += (int(simboli[i]) - int(simboli[i+1]))
			elif operandi[i] == '-' and i != 0:
				risultato = int(risultato) -  int(simboli[i+1])
			
			if operandi[i] == '*' and i == 0:
				risultato =  (int(simboli[i]) * int(simboli[i+1]))
			elif operandi[i] == '*' and i != 0:
				risultato = int(risultato) * int(simboli[i+1])
				
			if operandi[i] == '/' and i == 0:
				if simboli[i+1] != 0:
					risultato /= (float(int(simboli[i]) / int(simboli[i+1])))
			elif operandi[i] == '/' and i != 0:
				if simboli[i+1] != 0:
					risultato = risultato /  float(simboli[i+1])
				
			if simboli[i] == '^' and i == 0:
				risultato += int(simboli[i]) ** int(simboli[i+1])
			elif operandi[i] == '^' and i != 0:
				risultato = risultato ** int(simboli[i+1])
		except:
			break
	stringa += ' = ' + str(risultato)
	bot.sendMessage(chat_id,stringa,reply_to_message_id=mss_id)
	return

"""											
	La seguente funzione prende come parametri l'id dell'utente che ha chiamato la funzione, il contenuto del messaggio, l'array di tutti gli utenti registrati
	e l'array di tutti i nickname di tutti gli utenti registrati.
	
	La funzione permette l'invio di un messaggio in modalità anonima a un utente, il tutto purchè questo utente sia stato registrato dal bot in chat privata.
	Questa funzione viene chiamata tramite il comando /anon @username 'corpo del messaggio'
	
	Il messaggio che entra nella funzione è già tagliato per quanto riguarda la keyword '/anon', dunque sarà composto solo dal nickname e dal corpo del messaggio.
	Viene prima di tutto verificato che il messaggio inizi dunque con il carattere '@', cosi che venga inizializzata la ricerca del nickname tra tutti quelli
	registrati. In caso di riscontro negativo, la funzione terminerà con un messaggio.
	
	Una volta trovato il nickname, il bot procederà a inviare il messaggio desiderato all'utente scelto. Nel caso in cui invece non viene trovato il nickname
	tra quelli registrati, la funzione terminerà con un messaggio.
	Una volta inviato il messaggio, l'utente riceverà un messaggio di conferma dell'operazione andata a buon fine.
"""
def saraha(mess,messaggio,Utenti,Nickname):
	messaggio = messaggio[6:]
	if messaggio.startswith("@"):
			i = 0
			while True:
				if messaggio[i] == ' ':
					user = messaggio[0:i]
					messaggio = messaggio[i+1:]
					for k in range(len(Utenti)):
						if k == len(Utenti) -1:
							bot.sendMessage(mess,"L'utente desiderato non è stato trovato.\nProbabilmente non l'ho ancora potuto registrare.")
							break
						if user == Nickname[k].replace('\n',''):
							ut = Utenti[k]
							try:
								bot.sendMessage(ut,">>>>Messaggio arrivato in anonimo<<<<\n\n" + messaggio)
								bot.sendMessage(mess,"Messaggio anonimo inviato con successo!")
								print '>>>>Messaggio anonimo inviato correttamente<<<< ==> Utente: ' + user + '\n\n' +messaggio
								return
							except:
								bot.sendMessage(mess,"L'utente desiderato non mi ha ancora avviato.\nDi conseguenza non posso inviare alcun messaggio.")
								return
							break
						else:
							continue
					break
				else:
					i = i + 1
					continue
				break
	else:
		bot.sendMessage(mess,"Sintassi errata")
		return

"""

"""
def flame():
	return
