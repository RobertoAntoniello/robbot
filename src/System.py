# coding=utf-8
import time
import telepot
import telebot
import commands
import random
import traceback
import time
from datetime import date 
import operator
global bot
bot = telepot.Bot('133326326:AAHCjdG48vALFV-8iqmIammHMbHWcEkx-mM')

"""
	La seguente funzione scrivi(utente,messaggio) permette di scrivere a un utente nei panni di RobBot.
	
	parametri: id utente desiderato, il messaggio da trasmettere.
	
	Una volta chiamata la funzione il bot procederà a inviare il messaggio all'utente selezionato. 
	Per inviare un nuovo messaggio è necessario ripetere la procedura.
"""
def scrivi(utente,messaggio) :
	try:
		bot.sendMessage(utente,messaggio)
		print '>>>>ID chat valido<<<< ==> Messaggio inviato correttamente a ' + str(utente)
		return
	except:
		print '>>>>ID chat non valido<<<< ==> Messaggio non inviato'
		return
			
"""
	La seguente funzione prende come parametro il nome di un file di testo, che sarà quello contenente tutti gli id delle chat in cui è presente il bot
   	invierà a tutte le chat un messaggio in cui si descrive il fatto che il bot è stato riavviato.
"""

def avvio(s):
	chat_gruppi = []
	file_gruppi = open(s,'r')
	chat = '.'
	while 1:
		chat = str(file_gruppi.readline())
		if chat == '':
			break
		chat = chat.replace('\n','')
		chat_gruppi.append(chat)
	file_gruppi.close()
	for i in range(len(chat_gruppi)):
		chat = int(chat_gruppi[i])
		bot.sendMessage(chat,"Sono stato riavviato, da questo momento inizierò a prepararmi per rispondere alle richieste.")
	return
	
"""
	La seguente funzione prende come parametro il nome di un file di testo, che sarà quello contenente tutti gli id delle chat in cui è presente il bot
	invierà a tutte le chat un messaggio in cui si descrive il fatto che il bot è stato arrestato.
"""

def arresto(s):
	chat_gruppi = []
	file_gruppi = open(s,'r')
	chat = '.'
	while 1:
		chat = str(file_gruppi.readline())
		if chat == '':
			break
		chat = chat.replace('\n','')
		chat_gruppi.append(chat)
	file_gruppi.close()
	for i in range(len(chat_gruppi)):
		chat = chat_gruppi[i]
		chat = int(chat)
		bot.sendMessage(chat,"Sono stato arrestato, potrete utilizzarmi quando verrò riavviato.")
	return

"""
	La seguente funzione serve a loggare ogni messaggio ricevuto dal bot in modo da tenere sotto traccia ogni singolo messaggio, in caso di bug o altro.
	
	parametri: id chat, nome utente, username di chi scrive, id utente di chi scrive, il messaggio scritto.
	
	Secondo un format specifico, ogni giorno viene creato un file di log in cui vengono salvate le informazioni date da parametro.
"""
def logger(chat,nome_utente,username,utente,messaggio):
	data_messaggio = time.strftime("%d/%m/%Y")
	orario_messaggio = time.strftime("%H:%M:%S")
	path = 'Files/Logger/Logger_' + data_messaggio.replace('/','_')
	try:
		file_logger = open(path,'r')
		file_logger.close()
		file_logger = open(path,'a')
		stringa = str(chat) + ";" + nome_utente + ";" + username + ";" + str(utente) + ";" + str(orario_messaggio) + ";" + messaggio + '\n\n'
		try:
			file_logger.write(stringa)
		except:
			file_logger.write(stringa.encode('utf-8'))
	except:
		print '>>>>Il file logger in data odierna non esiste<<<< ==> Scrivo la prima riga di default'
		file_logger = open(path,'w')
		file_logger.write('File di log data: ' + data_messaggio + '\n\nID_CHAT||||NOME UTENTE||||USERNAME||||ID_UTENTE||||ORA_MESSAGGIO||||MESSAGGIO\n\n')
		stringa = str(chat) + ";" + nome_utente + ";" + username + ";" + str(utente) + ";" + str(orario_messaggio) + ";" + messaggio + '\n\n'
		try:
			file_logger.write(stringa)
		except:
			file_logger.write(stringa.encode('utf-8'))
		file_logger.close()
	print '>>>>Log messaggio salvato con successo<<<<'
	return

"""
	La seguente funzione serve a filtrare gli oggetti di tipo file_id e a verificare di che tipo è il contenuto del messaggio.
	
	parametri: lista_formati che contiene i tipi di contenuto e il json di tutto il messaggio
	
	Ciclo infinito dove una volta verificato di che messaggio si tratta, termina restituendo il fileID. 
	Se il fileID è vuoto, significa che il messaggio è solo testo.
"""
def formati(lista_formati,msg):
	composto = '>>>>Il file multimediale inviato è composto forse da '
	NonComposto = '>>>>Il messaggio inviato non è composto da testo.<<<<'
	restart = True
	while restart:
		for i in range(len(lista_formati)):	
			if lista_formati[i] == 'text':
				try:
					messaggio = msg['text']
					restart = False
					return ''
				except:
					print NonComposto
			elif lista_formati[i] == 'photo':
				restart = False
				try:
					fileID = msg['photo'][-1]['file_id']
					print '>>>>Il messaggio inviato è composto da immagine<<<<'
					return msg['photo'][-1]['file_id']
				except:
					print ''
			else:
				print composto + lista_formati[i]+'<<<<'
				try:
					return msg[lista_formati[i]]['file_id']
				except:
					print 'Nuova iterazione formati==>'
				restart = False

"""

	La seguente funzione è necessaria per controllare che il bot sia ancora presente nelle chat registrare. Il controllo avviene in media ogni 30 secondi.
	Se la chat non è presente viene registrata, se viene riscontrato il ban del bot dalla chat, viene rimossa.

"""
def check_group_chat(chat_id):
	orario_ora = time.strftime("%H:%M:%S")
	file_orario = open('Files/Date/check_group_chat.txt','r')
	orario_confronto = file_orario.read()
	if (int(orario_ora[6:]) - int(orario_confronto[6:])) > 30 or (int(orario_ora[6:]) - int(orario_confronto[6:])) < 0:
		print '>>>>Controllo periodico delle chat salvate<<<<'
		chat_gruppi = []
		file_gruppi = open('chat_gruppi.txt','r')
		while 1:
			chat = str(file_gruppi.readline())
			if chat == '':
				break
			chat = chat.replace('\n','')
			chat_gruppi.append(chat)
		file_gruppi.close()
		print '/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-'
		numchat = len(chat_gruppi)
		for i in range(numchat):
			chat = int(chat_gruppi[i])
			try:
				if bot.getChat(chat):
					s = bot.getChatMember(chat,133326326)
					if s['status'] != 'left' and s['status'] != 'kicked':
						print '>>>>Sono ancora in questa chat<<<< ==>' + str(chat)
					else:
						chat_gruppi.remove(str(chat))
						print '>>>>chat rimossa<<<< ==> causa rimozione del bot'
						file_gruppi = open('chat_gruppi.txt','w')
						numchat = len(chat_gruppi)
						for i in range(len(chat_gruppi)):
							chat = chat_gruppi[i] + '\n'
							file_gruppi.write(chat)
						file_gruppi.close()
				if i == numchat-1 and chat_id != mess:
					chat = str(chat_id) + '\n'
					chat_gruppi.append(chat)
					file_gruppi = open('chat_gruppi.txt','a')
					file_gruppi.write(chat)
					file_gruppi.close()
					print '>>>>nuova chat aggiunta<<<<'
					chat = chat.replace('\n','')
					chat = int(chat)
					bot.sendMessage(chat,'Nuova chat registrata! Se volete conoscermi meglio, avviatemi in privato!\n\nSe avete idee per funzionalità ulteriori, scrivetemi in privato. Il mio creatore sarà lieto di leggere e valutarne la fattibilità.')
					break
			except:
				break
		print '/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-/*-'
		file_orario = open('Files/Date/check_group_chat.txt','w')
		file_orario.write(orario_ora)
		file_orario.close()
	return
